<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendatangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendatangs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('penduduk_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->date('tanggal_datang');
            $table->string('alamat_asal');
            $table->string('alamat_sekarang');
            $table->string('alasan');
            $table->string('desa');
            $table->string('kecamatan');
            $table->string('kabupaten');
            $table->string('provinsi');
            $table->string('jml_ikut');
            $table->string('jenis_kelamin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendatangs');
    }
}
