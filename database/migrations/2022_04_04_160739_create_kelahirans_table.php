<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelahiransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelahirans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('kelurahans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('dusuns')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_anak');
            $table->string('anak_ke');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['Laki-Laki', 'Perempuan']);
            $table->string('agama');
            $table->string('alamat');
            $table->string('panjang');
            $table->string('berat');
            $table->string('kondisi_lahir');
            $table->string('jenis_kelahiran');
            $table->string('tempat_melahirkan');
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->string('kewarganegaraan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelahirans');
    }
}
