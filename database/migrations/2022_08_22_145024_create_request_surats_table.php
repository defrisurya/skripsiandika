<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestSuratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_surats', function (Blueprint $table) {
            $table->id();
            $table->string('request_code');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama')->nullable();
            $table->string('foto_ktp')->nullable();
            $table->string('foto_kk');
            $table->string('keterangan');
            $table->string('pelapor')->nullable();
            $table->string('saksi1')->nullable();
            $table->string('saksi2')->nullable();
            $table->string('tempat_kematian')->nullable();
            $table->date('tanggal_kematian')->nullable();
            $table->date('tanggal_pelaporan')->nullable();
            $table->time('jam_kematian')->nullable();
            $table->string('penyebab_kematian')->nullable();
            $table->string('menerangkan')->nullable();
            $table->string('desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('alamat_tujuan')->nullable();
            $table->string('alasan')->nullable();
            $table->string('jml_ikut')->nullable();
            $table->string('terdahulu')->nullable();
            $table->string('jenis_usaha')->nullable();
            $table->string('alamat_usaha')->nullable();
            $table->string('nama_usaha')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_surats');
    }
}
