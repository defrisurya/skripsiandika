<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratnikahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suratnikah', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('penduduk_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('nama_ayah')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('nama_ibu')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->string('terdahulu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suratnikah');
    }
}
