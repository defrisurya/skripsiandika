<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenduduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penduduks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('kelurahans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('dusuns')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('kk_id')->constrained('k_k_s')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nik');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['Laki-Laki', 'Perempuan']);
            $table->string('agama');
            $table->string('golongan_darah');
            $table->string('status_perkawinan');
            $table->string('pekerjaan');
            $table->string('kewarganegaraan');
            $table->string('pendidikan');
            $table->string('hubungan_keluarga');
            $table->enum('status_kependudukan', ['Aktif', 'Belum Kawin', 'Cerai Hidup', 'Cerai Mati', 'Pindah', 'Pendatang', 'Meninggal']);
            $table->string('rt');
            $table->string('rw');
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penduduks');
    }
}
