<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKematiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kematians', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('kelurahans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('dusuns')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('penduduk_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('kk_id')->constrained('k_k_s')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('ayah')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('ibu')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('pelapor')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('saksi1')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('saksi2')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->string('tempat_kematian');
            $table->date('tanggal_kematian');
            $table->date('tanggal_pelaporan');
            $table->time('jam_kematian');
            $table->string('penyebab_kematian');
            $table->string('jenis_kelamin');
            $table->string('menerangkan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kematians');
    }
}
