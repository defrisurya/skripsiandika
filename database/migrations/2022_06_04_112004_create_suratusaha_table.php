<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratusahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suratusaha', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kelurahan_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dusun_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('penduduk_id')->constrained('penduduks')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nik')->unique();
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('warganegara');
            $table->string('agama');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->string('jenis_usaha');
            $table->string('alamat_usaha');
            $table->string('nama_usaha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suratusaha');
    }
}
