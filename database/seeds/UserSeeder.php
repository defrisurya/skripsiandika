<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Seeder Table User */
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'role' => 'admin',
                'password' => Hash::make('password'),
            ],
            [
                'name' => 'Karyawan',
                'email' => 'karyawan@mail.com',
                'role' => 'karyawan',
                'password' => Hash::make('password'),
            ],
            [
                'name' => 'Lurah',
                'email' => 'lurah@mail.com',
                'role' => 'lurah',
                'password' => Hash::make('password'),
            ],
            [
                'name' => 'Masyarakat',
                'email' => 'masyarakat@mail.com',
                'role' => 'masyarakat',
                'password' => Hash::make('password'),
            ]
        ]);

        /* Seeder Table Kelurahan */
        DB::table('kelurahans')->insert([
            [
                'nama_kelurahan' => 'Sindumartani',
            ]
        ]);

        /* Seeder Table Slider */
        DB::table('sliders')->insert([
            [
                'foto1' => 'front/home/img/slider/slider1.jpg',
                'foto2' => 'front/home/img/slider/slider2.jpg',
                'foto3' => 'front/home/img/slider/slider3.jpg',
            ]
        ]);

        /* Seeder Table Profile */
        DB::table('profiles')->insert([
            [
                'alamat' => 'Sorobayan, Sindumartani, Ngemplak, Sleman, Yogyakarta Kapanewon Ngemplak Kabupaten Sleman Provinsi DI Yogyakarta Kode Pos 55584',
                'tlp' => '085865753000',
                'email' => 'sindumartani@gmail.com',
                'jam_kerja_awal' => '08:00:00',
                'jam_kerja_akhir' => '14:00:00',
                'maps' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31631.170037092383!2d110.45685935141655!3d-7.694282115594917!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5c89e5001c63%3A0x5027a76e3569600!2sSindumartani%2C%20Kec.%20Ngemplak%2C%20Kabupaten%20Sleman%2C%20Daerah%20Istimewa%20Yogyakarta!5e0!3m2!1sid!2sid!4v1660719376514!5m2!1sid!2sid',
            ]
        ]);

        /* Seeder Table Dusun */
        DB::table('dusuns')->insert([
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Jelapan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Pencar',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Morangan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Kentingan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Tambakan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Kejambon Lor',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Kejambon Kidul',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Ngasem',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Koripan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Bokesan',
            ],
            [
                'kelurahan_id' => '1',
                'nama_dusun' => 'Kayen',
            ],
        ]);
    }
}
