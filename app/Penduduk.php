<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penduduk extends Model
{
    protected $table = 'penduduks';
    protected $fillable = [
        'kelurahan_id',
        'dusun_id',
        'kk_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'agama',
        'golongan_darah',
        'status_perkawinan',
        'pekerjaan',
        'kewarganegaraan',
        'pendidikan',
        'hubungan_keluarga',
        'status_kependudukan',
        'rt',
        'rw',
        'nama_ayah',
        'nama_ibu',
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }

    public function kk()
    {
        return $this->belongsTo(KK::class, 'kk_id');
    }
}
