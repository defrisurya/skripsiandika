<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dusun extends Model
{
    protected $table = 'dusuns';
    protected $fillable = [
        'kelurahan_id',
        'nama_dusun'
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function kk()
    {
        return $this->hasMany(KK::class);
    }

    public function penduduk()
    {
        return $this->hasMany(Penduduk::class, 'id');
    }

    public function pendatang()
    {
        return $this->hasMany(Pendatang::class);
    }

    public function kematian()
    {
        return $this->hasMany(Kematian::class);
    }

    public function kelahiran()
    {
        return $this->hasMany(Kelahiran::class);
    }

    public function pindah()
    {
        return $this->hasMany(Pindah::class);
    }

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }
}
