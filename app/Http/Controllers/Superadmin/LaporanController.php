<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Dusun;
use App\Kelurahan;
use App\Kematian;
use App\Pendatang;
use App\Pindah;
use App\Suratnikah;
use App\Suratusaha;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function viewLaporan()
    {
        return view('Superadmin.laporan.view');
    }

    public function index()
    {
        $dusun = Dusun::all();
        $kelurahan = Kelurahan::first();
        return view('Superadmin.laporan.index', compact(
            'dusun',
            'kelurahan',
        ));
    }
    public function printKematian()
    {
        $kelurahan = Kelurahan::first();
        $kematian = Kematian::all();
        return view('Superadmin.laporan.lapKematian', compact(
            'kelurahan',
            'kematian',
        ));
    }
    public function printPindah()
    {
        $kelurahan = Kelurahan::first();
        $pindah = Pindah::all();
        return view('Superadmin.laporan.lapPindah', compact(
            'pindah',
            'kelurahan',
        ));
    }
    public function printPendatang()
    {
        $kelurahan = Kelurahan::first();
        $pendatang = Pendatang::all();
        return view('Superadmin.laporan.lapPendatang', compact(
            'pendatang',
            'kelurahan',
        ));
    }
    public function printKetUsaha()
    {
        $kelurahan = Kelurahan::first();
        $usaha = Suratusaha::all();
        return view('Superadmin.laporan.lapUsaha', compact(
            'usaha',
            'kelurahan',
        ));
    }
    public function printNikah()
    {
        $kelurahan = Kelurahan::first();
        $nikah = Suratnikah::all();
        return view('Superadmin.laporan.lapNikah', compact(
            'nikah',
            'kelurahan',
        ));
    }
}
