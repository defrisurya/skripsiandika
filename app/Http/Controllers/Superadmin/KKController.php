<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\KK;
use App\Kelurahan;
use App\Dusun;
use App\Penduduk;
use Illuminate\Http\Request;

class KKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KK::paginate(10);
        return view('Superadmin.kk.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.kk.create', compact('kelurahan', 'dusun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'no_kk' => 'required',
            'alamat' => 'required',
            'nama_kepalakeluarga' => 'required',
        ]);

        $data = $request->all();
        $kk = new KK;
        $kk->kelurahan_id = $data['kelurahan_id'];
        $kk->dusun_id = $data['dusun_id'];
        $kk->no_kk = $data['no_kk'];
        $kk->nama_kepalakeluarga = $data['nama_kepalakeluarga'];
        $kk->alamat = $data['alamat'];
        $kk->save();

        foreach ($data['nik'] as $item => $value) {
            $data2 = array(
                'kelurahan_id' => $data['kelurahan_id'],
                'dusun_id' => $data['dusun_id'],
                'kk_id' => $kk->id,
                'nik' => $data['nik'][$item],
                'nama' => $data['nama'][$item],
                'tempat_lahir' => $data['tempat_lahir'][$item],
                'tanggal_lahir' => $data['tanggal_lahir'][$item],
                'jenis_kelamin' => $data['jenis_kelamin'][$item],
                'agama' => $data['agama'][$item],
                'pendidikan' => $data['pendidikan'][$item],
                'pekerjaan' => $data['pekerjaan'][$item],
                'status_perkawinan' => $data['status_perkawinan'][$item],
                'golongan_darah' => $data['golongan_darah'][$item],
                'hubungan_keluarga' => $data['hubungan_keluarga'][$item],
                'kewarganegaraan' => $data['kewarganegaraan'][$item],
                'rt' => $data['rt'][$item],
                'rw' => $data['rw'][$item],
                'nama_ayah' => $data['nama_ayah'][$item],
                'nama_ibu' => $data['nama_ibu'][$item],
                'status_kependudukan' => 'Aktif'
            );
            // dd($data2);
            Penduduk::create($data2);
        }

        toast('Data Kartu Keluarga berhasil ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('kk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KK::with('penduduk')->findOrFail($id);
        // dd($data);
        return view('Superadmin.kk.show', compact('data'));
    }

    public function add($id)
    {
        $data = KK::with('penduduk')->findOrFail($id);
        // dd($data);
        return view('Superadmin.kk.tambah', compact('data'));
    }

    public function add_store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        foreach ($data['nik'] as $item => $value) {
            $data2 = array(
                'kelurahan_id' => $data['kelurahan_id'],
                'dusun_id' => $data['dusun_id'],
                'kk_id' => $data['kk_id'],
                'nik' => $data['nik'][$item],
                'nama' => $data['nama'][$item],
                'tempat_lahir' => $data['tempat_lahir'][$item],
                'tanggal_lahir' => $data['tanggal_lahir'][$item],
                'alamat' => $data['alamat'][$item],
                'jenis_kelamin' => $data['jenis_kelamin'][$item],
                'agama' => $data['agama'][$item],
                'pendidikan' => $data['pendidikan'][$item],
                'pekerjaan' => $data['pekerjaan'][$item],
                'status_perkawinan' => $data['status_perkawinan'][$item],
                'golongan_darah' => $data['golongan_darah'][$item],
                'hubungan_keluarga' => $data['hubungan_keluarga'][$item],
                'kewarganegaraan' => $data['kewarganegaraan'][$item],
                'rt' => $data['rt'][$item],
                'rw' => $data['rw'][$item],
                'nama_ayah' => $data['nama_ayah'][$item],
                'nama_ibu' => $data['nama_ibu'][$item],
                'status_kependudukan' => 'Aktif'
            );
            // dd($data2);
            Penduduk::create($data2);
        }

        toast('Anggota Keluarga berhasil ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('kk.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = KK::findOrFail($id);
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.kk.edit', compact('data', 'kelurahan', 'dusun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'no_kk' => 'required',
            'nama_kepalakeluarga' => 'required',
        ]);

        $data = $request->all();
        KK::findOrFail($id)->update($data);

        toast('Data Kartu Keluarga berhasil diubah', 'success')->position('bottom-end');
        return redirect()->route('kk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        KK::findOrFail($id)->delete();

        toast('Data Kartu Keluarga berhasil dihapus', 'error')->position('bottom-end');
        return redirect()->route('kk.index');
    }
}
