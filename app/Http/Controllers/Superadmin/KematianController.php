<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Kematian;
use App\Kelurahan;
use App\Dusun;
use App\Penduduk;
use App\KK;
use App\RequestSurat;
use Illuminate\Http\Request;

class KematianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kematian::paginate(10);
        return view('Superadmin.kematian.index', compact('data'));
    }

    public function print($id)
    {
        $data = Kematian::findOrFail($id);
        $kelurahan = Kelurahan::first();
        return view('Superadmin.kematian.print', compact('data', 'kelurahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        $penduduk = Penduduk::all();
        $kk = KK::all();
        return view('Superadmin.kematian.create', compact('kelurahan', 'dusun', 'penduduk', 'kk'));
    }

    public function autofill(Request $request)
    {
        $penduduk_id = $request->search;

        if ($penduduk_id == '') {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->get();
        } else {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->where('nik', 'like', '%' . $penduduk_id . '%')->get();
        }

        $response = array();
        foreach ($penduduks as $penduduk) {
            $response[] = array(
                "value" => $penduduk->kk->no_kk,
                "label" => $penduduk->nik . ' - ' . $penduduk->nama,
                "id" => $penduduk->id,
                "kk_id" => $penduduk->kk_id,
                "ayah" => $penduduk->nama_ayah,
                "ibu" => $penduduk->nama_ibu,
                "jk" => $penduduk->jenis_kelamin,
            );
        }

        return response()->json($response);
    }

    public function autofillfromrequest(Request $request)
    {
        $request_code = $request->cari;
        
        if ($request_code == '') {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'tempat_kematian', 'id', 'tanggal_kematian', 'jam_kematian', 'penyebab_kematian', 'menerangkan', 'tanggal_pelaporan')->get();
        } else {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'tempat_kematian', 'id', 'tanggal_kematian', 'jam_kematian', 'penyebab_kematian', 'menerangkan', 'tanggal_pelaporan')->where('request_code', 'like', '%' . $request_code . '%')->get();
            // dd($requests);
        }

        $result = array();
        foreach ($requests as $req) {
            $result[] = array(
                "label" => $req->request_code,
                "value" => $req->tanggal_pelaporan,
                "tmp_mati" => $req->tempat_kematian,
                "tgl_mati" => $req->tanggal_kematian,
                "jam_mati" => $req->jam_kematian,
                "sebab_mati" => $req->penyebab_kematian,
                "menerangkan" => $req->menerangkan,
            );
        }

        return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'tanggal_kematian' => 'required',
            'tanggal_pelaporan' => 'required',
            'tempat_kematian' => 'required',
            'penyebab_kematian' => 'required',
            'jam_kematian' => 'required',
            'ayah' => 'required',
            'ibu' => 'required',
            'pelapor' => 'required',
            'kk_id' => 'required',
            'saksi1' => 'required',
            'saksi2' => 'required',
            'jenis_kelamin' => 'required',
        ]);
        // dd($request->all());
        $data = new Kematian;
        $data->kelurahan_id = $request->kelurahan_id;
        $data->dusun_id = $request->dusun_id;
        $data->penduduk_id = $request->penduduk_id;
        $data->tanggal_kematian = $request->tanggal_kematian;
        $data->tanggal_pelaporan = $request->tanggal_pelaporan;
        $data->tempat_kematian = $request->tempat_kematian;
        $data->penyebab_kematian = $request->penyebab_kematian;
        $data->jam_kematian = $request->jam_kematian;
        $data->ayah = $request->ayah;
        $data->ibu = $request->ibu;
        $data->pelapor = $request->pelapor;
        $data->kk_id = $request->kk_id;
        $data->saksi1 = $request->saksi1;
        $data->saksi2 = $request->saksi2;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->menerangkan = $request->menerangkan;
        $data->save();

        Penduduk::where('id', $data->penduduk_id)->update([
            'status_kependudukan' => 'Meninggal',
        ]);

        toast('Data Kematian Berhasil Ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('kematian.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kematian::findOrFail($id);
        return view('Superadmin.kematian.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        $penduduk = Penduduk::all();
        $kk = KK::all();
        $data = Kematian::findOrFail($id);
        return view('Superadmin.kematian.edit', compact('data', 'kelurahan', 'dusun', 'penduduk', 'kk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'tanggal_kematian' => 'required',
            'tanggal_pelaporan' => 'required',
            'tempat_kematian' => 'required',
            'penyebab_kematian' => 'required',
            'jam_kematian' => 'required',
            'ayah' => 'required',
            'ibu' => 'required',
            'pelapor' => 'required',
            'kk_id' => 'required',
            'saksi1' => 'required',
            'saksi2' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        $data = $request->all();
        // dd($data);
        Kematian::findOrFail($id)->update($data);

        toast('Data Kematian Berhasil Diubah', 'success')->position('bottom-end');
        return redirect()->route('kematian.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
