<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Kelahiran;
use App\Kelurahan;
use App\Dusun;
use App\Penduduk;
use Illuminate\Http\Request;

class KelahiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kelahiran::with('penduduk')->paginate(10);
        return view('Superadmin.kelahiran.index', compact('data'));
    }

    public function print($id)
    {
        $data = Kelahiran::findOrFail($id);
        return view('Superadmin.kelahiran.print', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penduduk = Penduduk::all();
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.kelahiran.create', compact('penduduk', 'kelurahan', 'dusun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'nama_anak' => 'required',
            'anak_ke' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'panjang' => 'required',
            'berat' => 'required',
            'kondisi_lahir' => 'required',
            'jenis_kelahiran' => 'required',
            'tempat_melahirkan' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'kewarganegaraan' => 'required',
        ]);

        $data = $request->all();
        Kelahiran::create($data);

        toast('Data Kelahiran berhasil ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('kelahiran.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kelahiran::findOrFail($id);
        return view('Superadmin.kelahiran.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kelahiran::findOrFail($id);
        $penduduk = Penduduk::all();
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.kelahiran.edit', compact('data', 'penduduk', 'kelurahan', 'dusun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'nama_anak' => 'required',
            'anak_ke' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'panjang' => 'required',
            'berat' => 'required',
            'kondisi_lahir' => 'required',
            'jenis_kelahiran' => 'required',
            'tempat_melahirkan' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'kewarganegaraan' => 'required',
        ]);

        $data = $request->all();
        Kelahiran::findOrFail($id)->update($data);

        toast('Data Kelahiran berhasil diubah', 'success')->position('bottom-end');
        return redirect()->route('kelahiran.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
