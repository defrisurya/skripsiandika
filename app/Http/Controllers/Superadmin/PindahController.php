<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Kelurahan;
use App\Dusun;
use App\Penduduk;
use App\Pindah;
use App\RequestSurat;
use Illuminate\Http\Request;

class PindahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pindah::paginate(10);
        return view('Superadmin.pindah.index', compact('data'));
    }

    public function print($id)
    {
        $data = Pindah::findOrFail($id);
        return view('Superadmin.pindah.print', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        $penduduk = Penduduk::all();
        return view('Superadmin.pindah.create', compact('kelurahan', 'dusun', 'penduduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'alamat_tujuan' => 'required',
            'tgl_pindah' => 'required',
            'alasan' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' => 'required',
            'jml_ikut' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        $data = $request->all();
        Pindah::create($data);

        Penduduk::where('id', $data['penduduk_id'])->update([
            'status_kependudukan' => 'Pindah',
        ]);

        toast('Data Pindah berhasil ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('pindah.index');
    }

    public function autofill(Request $request)
    {
        $penduduk_id = $request->search;

        if ($penduduk_id == '') {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->get();
        } else {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->where('nik', 'like', '%' . $penduduk_id . '%')->get();
        }

        $response = array();
        foreach ($penduduks as $penduduk) {
            $response[] = array(
                "value" => $penduduk->kk->no_kk,
                "label" => $penduduk->nik . ' - ' . $penduduk->nama,
                "id" => $penduduk->id,
                "kk_id" => $penduduk->kk_id,
                "ayah" => $penduduk->nama_ayah,
                "ibu" => $penduduk->nama_ibu,
                "jk" => $penduduk->jenis_kelamin,
            );
        }

        return response()->json($response);
    }

    public function autofillfromrequest(Request $request)
    {
        $request = $request->cari;

        if ($request == '') {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'desa', 'kecamatan', 'kabupaten', 'provinsi', 'alamat_tujuan', 'alasan', 'jml_ikut')->get();
        } else {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'desa', 'kecamatan', 'kabupaten', 'provinsi', 'alamat_tujuan', 'alasan', 'jml_ikut')->where('request_code', 'like', '%' . $request . '%')->get();
            // dd($requests);
        }

        $response = array();
        foreach ($requests as $req) {
            $response[] = array(
                "label" => $req->request_code,
                "value" => $req->desa,
                "kec" => $req->kecamatan,
                "kab" => $req->kabupaten,
                "prov" => $req->provinsi,
                "alamat" => $req->alamat_tujuan,
                "alasan" => $req->alasan,
                "jml_ikut" => $req->jml_ikut,
            );
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pindah::findOrFail($id);
        return view('Superadmin.pindah.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        $penduduk = Penduduk::all();
        $data = Pindah::findOrFail($id);
        return view('Superadmin.pindah.edit', compact('kelurahan', 'dusun', 'penduduk', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'alamat_tujuan' => 'required',
            'tgl_pindah' => 'required',
            'alasan' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' => 'required',
            'jml_ikut' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        $data = $request->all();
        Pindah::findOrFail($id)->update($data);

        toast('Data Pindah berhasil diubah', 'success')->position('bottom-end');
        return redirect()->route('pindah.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
