<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Suratnikah;
use App\Penduduk;
use App\Dusun;
use App\Kelurahan;
use App\RequestSurat;
use Illuminate\Http\Request;

class SuratnikahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Suratnikah::paginate(10);
        return view('Superadmin.suratnikah.index', compact('data'));
    }

    public function print($id)
    {
        $data = Suratnikah::findOrFail($id);
        $nikah = Penduduk::with('kk')
            ->where('id', $data->penduduk_id)
            ->first();
        $kelurahan = Kelurahan::where('id', $data->kelurahan_id)->first();
        $dusun = Dusun::where('id', $data->dusun_id)->first();
        return view('Superadmin.suratnikah.print', compact('data', 'nikah', 'kelurahan', 'dusun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penduduk = Penduduk::all();
        $dusun = Dusun::all();
        $kelurahan = Kelurahan::first();
        return view('Superadmin.suratnikah.create', compact('penduduk', 'dusun', 'kelurahan'));
    }

    public function autofillsuratnikah(Request $request)
    {
        $penduduk_id = $request->search;

        if ($penduduk_id == '') {
            $penduduks = Penduduk::orderby('nik', 'asc')
                ->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id')
                ->get();
        } else {
            $penduduks = Penduduk::orderby('nik', 'asc')
                ->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id')
                ->where('nik', 'like', '%' . $penduduk_id . '%')
                ->get();
        }

        $response = array();
        foreach ($penduduks as $penduduk) {
            $response[] = array(
                "value" => $penduduk->nik,
                "label" => $penduduk->nik . ' - ' . $penduduk->nama,
                "id" => $penduduk->id,
                "ayah" => $penduduk->nama_ayah,
                "ibu" => $penduduk->nama_ibu,
            );
        }

        return response()->json($response);
    }

    public function autofillfromrequest(Request $request)
    {
        $request = $request->cari;

        if ($request == '') {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'terdahulu')->get();
        } else {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'terdahulu')->where('request_code', 'like', '%' . $request . '%')->get();
            // dd($requests);
        }

        $response = array();
        foreach ($requests as $req) {
            $response[] = array(
                "label" => $req->request_code,
                "value" => $req->terdahulu,
            );
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
        ]);

        $data = $request->all();
        Suratnikah::create($data);

        toast('Data Surat Nikah Berhasil Dibuat', 'success')->position('bottom-end');
        return redirect()->route('suratnikah.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Suratnikah::findOrFail($id);
        return view('Superadmin.suratnikah.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Suratnikah::findOrFail($id);
        $penduduk = Penduduk::all();
        $dusun = Dusun::all();
        $kelurahan = Kelurahan::first();
        return view('Superadmin.suratnikah.edit', compact('data', 'penduduk', 'dusun', 'kelurahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
        ]);

        $data = $request->all();
        Suratnikah::findOrFail($id)->update($data);

        toast('Data Surat Nikah Berhasil Diubah', 'success')->position('bottom-end');
        return redirect()->route('suratnikah.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
