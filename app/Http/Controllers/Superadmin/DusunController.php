<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Dusun;
use App\Kelurahan;
use Illuminate\Http\Request;

class DusunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dusun::paginate(10);
        $kelurahan = Kelurahan::first();
        return view('Superadmin.dusun.index', compact('data', 'kelurahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'nama_dusun' => 'required',
        ]);

        $data = $request->all();
        Dusun::create($data);

        toast('Data Dusun berhasil ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('dusun.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Dusun::findOrFail($id);
        $kelurahan = Kelurahan::first();
        return view('Superadmin.dusun.edit', compact('data', 'kelurahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'nama_dusun' => 'required',
        ]);

        $data = $request->all();
        Dusun::findOrFail($id)->update($data);

        toast('Data Dusun berhasil diubah', 'success')->position('bottom-end');
        return redirect()->route('dusun.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dusun::findOrFail($id)->delete();

        toast('Data Dusun telah dihapus', 'error')->position('bottom-end');
        return redirect()->route('dusun.index');
    }
}
