<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Suratusaha;
use App\Kelurahan;
use App\Dusun;
use App\Penduduk;
use App\RequestSurat;
use Illuminate\Http\Request;

class SuratusahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Suratusaha::paginate(10);
        return view('Superadmin.suratusaha.index', compact('data'));
    }

    public function print($id)
    {
        $data = Suratusaha::findOrFail($id);
        return view('Superadmin.suratusaha.print', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.suratusaha.create', compact('kelurahan', 'dusun'));
    }

    public function autofillsuratusaha(Request $request)
    {
        $penduduk_id = $request->search;

        if ($penduduk_id == '') {
            $penduduks = Penduduk::orderby('nik', 'asc')
                ->select('nik', 'nama', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'agama', 'kewarganegaraan', 'pekerjaan', 'kk_id', 'id')
                ->get();
        } else {
            $penduduks = Penduduk::orderby('nik', 'asc')
                ->select('nik', 'nama', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'agama', 'kewarganegaraan', 'pekerjaan', 'kk_id', 'id')
                ->where('nik', 'like', '%' . $penduduk_id . '%')
                ->get();
            // dd($penduduks->kk);
        }

        $response = array();
        foreach ($penduduks as $penduduk) {
            $response[] = array(
                "value" => $penduduk->nik,
                "label" => $penduduk->nik . ' - ' . $penduduk->nama,
                "id" => $penduduk->id,
                "jk" => $penduduk->jenis_kelamin,
                "tl" => $penduduk->tempat_lahir,
                "tgl" => $penduduk->tanggal_lahir,
                "agama" => $penduduk->agama,
                "warganegara" => $penduduk->kewarganegaraan,
                "kerja" => $penduduk->pekerjaan,
                "alamat" => $penduduk->kk->alamat,
            );
        }

        return response()->json($response);
    }

    public function autofillfromrequest(Request $request)
    {
        $request = $request->cari;

        if ($request == '') {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'jenis_usaha', 'alamat_usaha', 'nama_usaha')->get();
        } else {
            $requests = RequestSurat::orderby('request_code', 'asc')->select('request_code', 'jenis_usaha', 'alamat_usaha', 'nama_usaha')->where('request_code', 'like', '%' . $request . '%')->get();
            // dd($requests);
        }

        $response = array();
        foreach ($requests as $req) {
            $response[] = array(
                "label" => $req->request_code,
                "value" => $req->jenis_usaha,
                "addres" => $req->alamat_usaha,
                "nama" => $req->nama_usaha,
            );
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'nik' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'warganegara' => 'required',
            'agama' => 'required',
            'pekerjaan' => 'required',
            'alamat' => 'required',
            'jenis_usaha' => 'required',
            'alamat_usaha' => 'required',
            'nama_usaha' => 'required',
        ]);

        Suratusaha::create($request->all());

        toast('Data Surat Keterangan Usaha Berhasil Dibuat', 'success')->position('bottom-end');
        return redirect()->route('suratusaha.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Suratusaha::findOrFail($id);
        return view('Superadmin.suratusaha.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Suratusaha::with('penduduk')->findOrFail($id);
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.suratusaha.edit', compact('data', 'kelurahan', 'dusun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'nik' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'warganegara' => 'required',
            'agama' => 'required',
            'pekerjaan' => 'required',
            'alamat' => 'required',
            'jenis_usaha' => 'required',
            'alamat_usaha' => 'required',
            'nama_usaha' => 'required',
        ]);
        $data = $request->all();
        Suratusaha::findOrFail($id)->update($data);

        toast('Data Surat Keterangan Usaha Berhasil Diubah', 'success')->position('bottom-end');
        return redirect()->route('suratusaha.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
