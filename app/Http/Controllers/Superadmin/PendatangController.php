<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Pendatang;
use App\Penduduk;
use App\Kelurahan;
use App\Dusun;
use Illuminate\Http\Request;

class PendatangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pendatang::paginate(10);
        return view('Superadmin.pendatang.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        $penduduk = Penduduk::all();
        return view('Superadmin.pendatang.create', compact('kelurahan', 'dusun', 'penduduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'tanggal_datang' => 'required',
            'alamat_asal' => 'required',
            'alamat_sekarang' => 'required',
            'alasan' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' => 'required',
            'jml_ikut' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        $data = $request->all();
        Pendatang::create($data);

        Penduduk::where('id', $data['penduduk_id'])->update([
            'status_kependudukan' => 'Pendatang'
        ]);

        toast('Data Pendatang Berhasil Ditambahkan', 'success')->position('bottom-end');
        return redirect()->route('pendatang.index');
    }

    public function autofill(Request $request)
    {
        $penduduk_id = $request->search;

        if ($penduduk_id == '') {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->get();
        } else {
            $penduduks = Penduduk::orderby('nik', 'asc')->select('kk_id', 'nik', 'nama', 'nama_ayah', 'nama_ibu', 'id', 'jenis_kelamin')->where('nik', 'like', '%' . $penduduk_id . '%')->get();
        }

        $response = array();
        foreach ($penduduks as $penduduk) {
            $response[] = array(
                "value" => $penduduk->kk->no_kk,
                "label" => $penduduk->nik . ' - ' . $penduduk->nama,
                "id" => $penduduk->id,
                "kk_id" => $penduduk->kk_id,
                "ayah" => $penduduk->nama_ayah,
                "ibu" => $penduduk->nama_ibu,
                "jk" => $penduduk->jenis_kelamin,
            );
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pendatang::findOrFail($id);
        return view('Superadmin.pendatang.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pendatang::findOrFail($id);
        $penduduk = Penduduk::all();
        $kelurahan = Kelurahan::first();
        $dusun = Dusun::all();
        return view('Superadmin.pendatang.edit', compact('data', 'kelurahan', 'dusun', 'penduduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kelurahan_id' => 'required',
            'dusun_id' => 'required',
            'penduduk_id' => 'required',
            'tanggal_datang' => 'required',
            'alamat_asal' => 'required',
            'alamat_sekarang' => 'required',
            'alasan' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' => 'required',
            'jml_ikut' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        $data = $request->all();
        Pendatang::findOrFail($id)->update($data);

        toast('Data Pendatang Berhasil Diubah', 'success')->position('bottom-end');
        return redirect()->route('pendatang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
