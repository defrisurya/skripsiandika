<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Profile;
use App\Dusun;

class FrontController extends Controller
{
    public function index()
    {
        $slider = Slider::first();
        $profil = Profile::first();
        $dusun = Dusun::all();

        return view('welcome', compact('slider', 'profil', 'dusun'));
    }

    public function surat_kelahiran()
    {
        $slider = Slider::first();
        $profil = Profile::first();

        return view('User.surat_kelahiran.index', compact('slider', 'profil'));
    }

    public function surat_kematian()
    {
        $slider = Slider::first();
        $profil = Profile::first();

        return view('User.surat_kematian.index', compact('slider', 'profil'));
    }

    public function surat_pindah()
    {
        $slider = Slider::first();
        $profil = Profile::first();

        return view('User.surat_pindah.index', compact('slider', 'profil'));
    }

    public function surat_nikah()
    {
        $slider = Slider::first();
        $profil = Profile::first();

        return view('User.penghantar_nikah.index', compact('slider', 'profil'));
    }

    public function surat_izin_usaha()
    {
        $slider = Slider::first();
        $profil = Profile::first();

        return view('User.izin_usaha.index', compact('slider', 'profil'));
    }
}
