<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RequestSurat;
use App\Profile;

class RequestSuratKelahiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = RequestSurat::where('user_id', auth()->user()->id)->get();
        $profil = Profile::first();

        return view('User.riwayat.index', compact('data', 'profil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'foto_ktp' => 'file|image|mimes:jpeg,png,jpg|max:1024|required',
            'foto_kk' => 'file|image|mimes:jpeg,png,jpg|max:1024|required',
        ]);

        $data = $request->all();

        $data['request_code'] = 'surat_kelahiran' . mt_rand(1, 99) . date('dmY');

        $foto_ktp = $request->foto_ktp;
        $new_foto = time() . 'surat_online_kelahiran' . $foto_ktp->getClientOriginalName();
        $tujuan_uploud = 'upload/surat_online_kelahiran/';
        $foto_ktp->move($tujuan_uploud, $new_foto);
        $data['foto_ktp'] = $tujuan_uploud . $new_foto;

        $foto_kk = $request->foto_kk;
        $new_foto = time() . 'surat_online_kelahiran' . $foto_kk->getClientOriginalName();
        $tujuan_uploud = 'upload/surat_online_kelahiran/';
        $foto_kk->move($tujuan_uploud, $new_foto);
        $data['foto_kk'] = $tujuan_uploud . $new_foto;

        // dd($data);

        RequestSurat::create($data);
        return redirect()->route('reqsuratonline.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = RequestSurat::findOrFail($id);
        $profil = Profile::first();

        return view('User.riwayat.show', compact('data', 'profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
