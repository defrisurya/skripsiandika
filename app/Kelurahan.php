<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahans';
    protected $fillable = ['nama_kelurahan'];

    public function dusun()
    {
        return $this->hasMany(Dusun::class);
    }

    public function penduduk()
    {
        return $this->hasMany(Penduduk::class);
    }

    public function kk()
    {
        return $this->hasMany(KK::class);
    }

    public function pendatang()
    {
        return $this->hasMany(Pendatang::class);
    }

    public function kematian()
    {
        return $this->hasMany(Kematian::class);
    }

    public function kelahiran()
    {
        return $this->hasMany(Kelahiran::class);
    }

    public function pindah()
    {
        return $this->hasMany(Pindah::class);
    }

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }
}
