<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KK extends Model
{
    protected $table = 'k_k_s';
    protected $fillable = [
        'kelurahan_id',
        'dusun_id',
        'no_kk',
        'nama_kepalakeluarga',
        'alamat',
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }

    public function penduduk()
    {
        return $this->hasMany(Penduduk::class, 'kk_id');
    }
}
