<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kematian extends Model
{
    protected $table = 'kematians';
    protected $fillable = [
        'kelurahan_id', 
        'dusun_id', 
        'penduduk_id',
        'kk_id',
        'tanggal_kematian',
        'tanggal_pelaporan',
        'tempat_kematian',
        'penyebab_kematian',
        'jam_kematian',
        'jenis_kelamin',
        'ayah',
        'ibu',
        'pelapor',
        'saksi1',
        'saksi2',
        'menerangkan'
    ];

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class, 'penduduk_id');
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }

    public function kk()
    {
        return $this->belongsTo(KK::class, 'kk_id');
    }
}
