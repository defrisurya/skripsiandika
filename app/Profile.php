<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = [
        'alamat',
        'tlp',
        'email',
        'jam_kerja_awal',
        'jam_kerja_akhir',
        'maps',
    ];
}
