<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratnikah extends Model
{
    protected $table = 'suratnikah';
    protected $fillable = ['kelurahan_id', 'dusun_id', 'penduduk_id', 'nama_ayah', 'nama_ibu', 'terdahulu'];

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class);
    }

    public function kk()
    {
        return $this->belongsTo(KK::class);
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class);
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class);
    }
}
