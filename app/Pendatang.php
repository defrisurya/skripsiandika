<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendatang extends Model
{
    protected $table = 'pendatangs';
    protected $fillable = [
        'kelurahan_id',
        'dusun_id',
        'penduduk_id',
        'tanggal_datang',
        'alamat_asal',
        'alamat_sekarang',
        'alasan',
        'desa',
        'kecamatan',
        'kabupaten',
        'provinsi',
        'jml_ikut',
        'jenis_kelamin',
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class);
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class);
    }
}
