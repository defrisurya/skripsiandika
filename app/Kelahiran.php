<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelahiran extends Model
{
    protected $table = 'kelahirans';
    protected $fillable = [
        'kelurahan_id', 
        'dusun_id',
        'nama_anak',
        'anak_ke',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'agama',
        'alamat',
        'panjang',
        'berat',
        'kondisi_lahir',
        'jenis_kelahiran',
        'tempat_melahirkan',
        'nama_ayah',
        'nama_ibu',
        'kewarganegaraan',
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class);
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class, 'id');
    }
}
