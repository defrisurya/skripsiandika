<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratusaha extends Model
{
    protected $table = 'suratusaha';
    protected $fillable = ['penduduk_id', 'nik', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'warganegara', 'agama', 'pekerjaan', 'alamat', 'jenis_usaha', 'alamat_usaha', 'kelurahan_id', 'dusun_id', 'nama_usaha'];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class);
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class);
    }

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class, 'penduduk_id');
    }
}
