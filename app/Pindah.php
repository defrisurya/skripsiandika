<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pindah extends Model
{
    protected $table = 'pindahs';
    protected $fillable = [
        'kelurahan_id',
        'dusun_id',
        'penduduk_id',
        'alamat_tujuan',
        'tgl_pindah',
        'alasan',
        'desa',
        'kecamatan',
        'kabupaten',
        'provinsi',
        'jml_ikut',
        'jenis_kelamin',
    ];

    public function penduduk()
    {
        return $this->belongsTo(Penduduk::class, 'penduduk_id', 'id');
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function dusun()
    {
        return $this->belongsTo(Dusun::class, 'dusun_id');
    }
}
