<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* User Interface */

Route::get('/', 'FrontController@index')->name('welcome');
Route::get('suratkelahiran', 'FrontController@surat_kelahiran')->name('suratkelahiran');
Route::get('suratkematian', 'FrontController@surat_kematian')->name('suratkematian');
Route::get('suratpindah', 'FrontController@surat_pindah')->name('suratpindah');
Route::get('suratpenghantarnikah', 'FrontController@surat_nikah')->name('suratnikah');
Route::get('suratizinusaha', 'FrontController@surat_izin_usaha')->name('suratizinusaha');

/* Request Surat Online */
Route::resource('reqsuratonline', 'User\RequestSuratKelahiranController');
Route::resource('reqsuratonlinedead', 'User\RequestSuratKematianController');
Route::resource('reqsuratonlinepindah', 'User\RequestSuratPindahController');
Route::resource('reqsuratonlinenikah', 'User\RequestSuratNikahController');
Route::resource('reqsuratonlineusaha', 'User\RequestSuratUsahaController');
/* End User Interface */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboardUser', 'HomeController@index')->name('user');

Route::group(['middleware' => ['auth' => 'CekRole:admin,karyawan,lurah'], 'prefix' => 'Superadmin'], function () {
    Route::namespace('Superadmin')->group(function () {
        Route::resource('kelurahan', 'kelurahanController');
        Route::resource('dusun', 'DusunController');
        Route::resource('kk', 'KKController');
        Route::get('add/{id}', 'KKController@add')->name('add');
        Route::post('addstore', 'KKController@add_store')->name('addstore');
        Route::resource('penduduk', 'PendudukController');
        Route::resource('kelahiran', 'KelahiranController');
        Route::resource('kematian', 'KematianController');
        Route::resource('pendatang', 'PendatangController');
        Route::resource('pindah', 'PindahController');
        Route::resource('staff', 'StaffController');
        Route::get('laporan', 'LaporanController@viewLaporan')->name('viewLaporan');
        Route::get('laporan/print/periodik', 'LaporanController@index')->name('LaporanPeriodik');
        Route::get('laporan/print/kematian', 'LaporanController@printKematian')->name('LaporanKematian');
        Route::get('laporan/print/pindah', 'LaporanController@printPindah')->name('LaporanPindah');
        Route::get('laporan/print/pendatang', 'LaporanController@printPendatang')->name('LaporanPendatang');
        Route::get('laporan/print/ketUsaha', 'LaporanController@printKetUsaha')->name('LaporanKetUsaha');
        Route::get('laporan/print/nikah', 'LaporanController@printNikah')->name('LaporanNikah');
        Route::resource('suratusaha', 'SuratusahaController');
        Route::resource('suratnikah', 'SuratnikahController');
        Route::resource('suratonline', 'ReqSuratOnlineController');
        Route::get('suratnikah/{id}/print', 'SuratnikahController@print')->name('suratnikah.print');
        Route::get('suratusaha/{id}/print', 'SuratusahaController@print')->name('suratusaha.print');
        Route::get('pindah/{id}/print', 'PindahController@print')->name('pindah.print');
        Route::get('kematian/{id}/print', 'KematianController@print')->name('kematian.print');
        Route::post('autofill', 'KematianController@autofill')->name('autofill');
        Route::post('autofillsuratusaha', 'SuratusahaController@autofillsuratusaha')->name('autofillsuratusaha');
        Route::post('autofillsuratnikah', 'SuratnikahController@autofillsuratnikah')->name('autofillsuratnikah');
        Route::post('autofillsuratpindah', 'PindahController@autofill')->name('autofillsuratpindah');
        Route::post('autofillsuratpendatang', 'PendatangController@autofill')->name('autofillsuratpendatang');

        // Autofill From Request
        Route::post('autofillfromreq', 'KematianController@autofillfromrequest')->name('autofillfromrequest');
        Route::post('autofillfromreqpindah', 'PindahController@autofillfromrequest')->name('autofillfromrequestpindah');
        Route::post('autofillfromreqnikah', 'SuratnikahController@autofillfromrequest')->name('autofillfromrequestnikah');
        Route::post('autofillfromrequsaha', 'SuratusahaController@autofillfromrequest')->name('autofillfromrequestusaha');

        /* Content Frontend */
        Route::resource('slider', 'SliderController');
        Route::resource('profile', 'ProfileController');
        /* End Content Frontend */
    });
});
