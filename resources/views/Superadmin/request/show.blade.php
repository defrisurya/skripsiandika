@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('suratonline.index') }}">Data Permintaan Surat Online</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Permintaan Surat Online</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <p>Permintaan Surat : {{ $data->keterangan }}</p>
                        <p>Atas Nama : {{ $data['user']->name }}</p>
                        <p>Kode Permintaan Surat : {{ $data->request_code }}</p>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <div class="form-group row mt-5">
                                    <div class="col">
                                        <label for="exampleInputUsername1">Foto KTP</label> <br>
                                        <a href="{{ asset($data->foto_ktp) }}" download>
                                            <img src="{{ asset($data->foto_ktp) }}" alt=""
                                                style="width: 150px; height:150px">
                                        </a>
                                        <small><i>Click Image For Download</i></small>
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputEmail1">Foto Kartu Keluarga</label> <br>
                                        <a href="{{ asset($data->foto_kk) }}" download>
                                            <img src="{{ asset($data->foto_kk) }}" alt=""
                                                style="width: 150px; height:150px">
                                        </a>
                                        <small><i>Click Image For Download</i></small>
                                    </div>
                                </div>

                                {{-- Kematian --}}
                                <div class="form-group row">
                                    @if ($data->pelapor != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Pelapor</label> <br>
                                            <span>{{ $data->pelapor }}</span>
                                        </div>
                                    @endif
                                    @if ($data->saksi1 != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Saksi 1</label> <br>
                                            <span>{{ $data->saksi1 }}</span>
                                        </div>
                                    @endif
                                    @if ($data->saksi2 != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Saksi 2</label> <br>
                                            <span>{{ $data->saksi2 }}</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    @if ($data->tempat_kematian != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Tempat Kematian</label> <br>
                                            <span>{{ $data->tempat_kematian }}</span>
                                        </div>
                                    @endif
                                    @if ($data->tanggal_kematian != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Tanggal Kematian</label> <br>
                                            <span>{!! date('d-M-Y', strtotime($data->tanggal_kematian)) !!}</span>
                                        </div>
                                    @endif
                                    @if ($data->jam_kematian != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Jam Kematian</label> <br>
                                            <span>{!! date('H:i', strtotime($data->jam_kematian)) !!} WIB</span>
                                        </div>
                                    @endif
                                    @if ($data->penyebab_kematian != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Penyebab</label> <br>
                                            <span>{{ $data->penyebab_kematian }}</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    @if ($data->menerangkan != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Menerangkan</label> <br>
                                            <span>{{ $data->menerangkan }}</span>
                                        </div>
                                    @endif
                                </div>

                                {{-- Pindah --}}
                                <div class="form-group row">
                                    @if ($data->desa != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Desa Tujuan</label> <br>
                                            <span>{{ $data->desa }}</span>
                                        </div>
                                    @endif
                                    @if ($data->kecamatan != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Kecamatan Tujuan</label> <br>
                                            <span>{{ $data->kecamatan }}</span>
                                        </div>
                                    @endif
                                    @if ($data->kabupaten != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Kabupaten Tujuan</label> <br>
                                            <span>{{ $data->kabupaten }}</span>
                                        </div>
                                    @endif
                                    @if ($data->provinsi != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Provinsi Tujuan</label> <br>
                                            <span>{{ $data->provinsi }}</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    @if ($data->alamat_tujuan != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Alamat Tujuan</label> <br>
                                            <span>{{ $data->alamat_tujuan }}</span>
                                        </div>
                                    @endif
                                    @if ($data->alasan != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Alasan</label> <br>
                                            <span>{{ $data->alasan }}</span>
                                        </div>
                                    @endif
                                    @if ($data->jml_ikut != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Jumlah Yang Ikut</label> <br>
                                            <span>{{ $data->jml_ikut }}</span>
                                        </div>
                                    @endif
                                </div>

                                {{-- Nikah --}}
                                <div class="form-group row">
                                    @if ($data->terdahulu != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Suami/Istri Terdahulu</label> <br>
                                            <span>{{ $data->terdahulu }}</span>
                                        </div>
                                    @endif
                                </div>

                                {{-- Usaha --}}
                                <div class="form-group row">
                                    @if ($data->jenis_usaha != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Jenis Usaha</label> <br>
                                            <span>{{ $data->jenis_usaha }}</span>
                                        </div>
                                    @endif
                                    @if ($data->alamat_usaha != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Alamat Usaha</label> <br>
                                            <span>{{ $data->alamat_usaha }}</span>
                                        </div>
                                    @endif
                                    @if ($data->nama_usaha != null)
                                        <div class="col">
                                            <label for="exampleInputConfirmPassword1">Nama Usaha</label> <br>
                                            <span>{{ $data->nama_usaha }}</span>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label for="exampleInputConfirmPassword1">Permintaan Surat</label> <br>
                                        <label class="badge bg-primary">{{ $data->keterangan }}</label>
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputConfirmPassword1">Status</label> <br>
                                        @if ($data->status != 'Selesai')
                                            <span class="badge bg-danger">{{ $data->status }}</span>
                                        @else
                                            <span class="badge bg-success text-dark">{{ $data->status }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <a href="{{ route('suratonline.index') }}" class="btn btn-primary"><i
                                                class="fas fa-arrow-left">&nbsp;Back</i>
                                        </a>
                                    </div>
                                    @if ($data->status != 'Selesai')
                                        <div class="col">
                                            <form method="POST" action="{{ route('suratonline.update', $data->id) }}">
                                                @csrf
                                                @method('PUT')
                                                <button type="submit" class="btn btn-success"><i class="fas fa-check">
                                                        &nbsp;Finish</i>
                                                </button>
                                            </form>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
