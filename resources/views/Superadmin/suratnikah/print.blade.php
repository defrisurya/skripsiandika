<!DOCTYPE html>
<html>

<head>
    <title>Cetak Surat Penghantar Nikah</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: center;
            font-size: 15px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }
    </style>
</head>

<body>
    <center>
        <table width="750">
            <tr>
                <td><img src="{{ asset('front') }}/home/img/kop_surat.png" width="100%" height="160"></td>
            </tr>
            <table width="750">
                <tr>
                    <td class="text2"><u><b>SURAT PENGANTAR NIKAH</b></u></td>
                </tr>
            </table>
        </table>
        <table width="750">
            <tr class="text2">
                <td>Nomor</td>
                <td width="750">: -</td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Yang bertanda tangan dibawah ini menjelaskan dengan sesungguhnya bahwa :</font>
                </td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr class="text2">
                <td>1. Nama</td>
                <td width="525">: <b>{{ $nikah->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $nikah->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Jenis Kelamin</td>
                <td width="525">: <b>{{ $nikah->jenis_kelamin }}</b></td>
            </tr>
            <tr>
                <td>4. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $nikah->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($nikah->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>5. Kewarganegaraan</td>
                <td width="525">: <b>{{ $nikah->kewarganegaraan }}</b></td>
            </tr>
            <tr>
                <td>6. Agama</td>
                <td width="525">: <b>{{ $nikah->agama }}</b></td>
            </tr>
            <tr>
                <td>7. Pekerjaan</td>
                <td width="525">: <b>{{ $nikah->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>8. Pendidikan Terakhir</td>
                <td width="525">: <b>{{ $nikah->pendidikan }}</b></td>
            </tr>
            <tr>
                <td>9. Alamat</td>
                <td width="525">: <b>{{ $nikah->kk->alamat }}</b></td>
            </tr>
            <tr>
                <td>10. Status Perkawinan</td>
                <td width="525">: <b>{{ $nikah->status_perkawinan }}</b></td>
            </tr>
            <tr>
                <td>11. Nama Suami/Istri Terdahulu</td>
                <td width="525">:&nbsp;
                    @if ($data->terdahulu != null)
                        <b>{{ $data->terdahulu }}</b>
                    @else
                        <b>-</b>
                    @endif
                </td>
            </tr>
        </table>
        <br>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Adalah benar anak dari perkawinan seorang pria :</font>
                </td>
            </tr>
        </table>
        <br>
        @php
            $ayah = App\Penduduk::with('kk')
                ->where('id', $data->nama_ayah)
                ->first();
        @endphp
        <table width="750">
            <tr class="text2">
                <td>Nama</td>
                <td width="525">: <b>{{ $ayah->nama_ayah }}</b></td>
            </tr>
            <tr>
                <td>Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $ayah->nik }}</b></td>
            </tr>
            <tr>
                <td>Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $ayah->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($ayah->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td width="525">: <b>{{ $ayah->kewarganegaraan }}</b></td>
            </tr>
            <tr>
                <td>Agama</td>
                <td width="525">: <b>{{ $ayah->agama }}</b></td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td width="525">: <b>{{ $ayah->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td width="525">: <b>{{ $ayah->kk->alamat }}</b></td>
            </tr>
        </table>
        <br>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Dengan seorang wanita :</font>
                </td>
            </tr>
        </table>
        <br>
        @php
            $ibu = App\Penduduk::with('kk')
                ->where('id', $data->nama_ibu)
                ->first();
        @endphp
        <table width="750">
            <tr class="text2">
                <td>Nama</td>
                <td width="525">: <b>{{ $ibu->nama_ibu }}</b></td>
            </tr>
            <tr>
                <td>Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $ibu->nik }}</b></td>
            </tr>
            <tr>
                <td>Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $ibu->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($ibu->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td width="525">: <b>{{ $ibu->kewarganegaraan }}</b></td>
            </tr>
            <tr>
                <td>Agama</td>
                <td width="525">: <b>{{ $ibu->agama }}</b></td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td width="525">: <b>{{ $ibu->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td width="525">: <b>{{ $ibu->kk->alamat }}</b></td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Demikian surat Pengantar ini dibuat dengan mengingat sumpah jabatan dan untuk
                        dipergunakan sebagaimana
                        mestinya.
                    </font>
                </td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td width="430"></td>
                <td class="text" align="center">Yogyakarta,
                    {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
            </tr>
            <tr>
                <td width="430"><br><br><br><br></td>
                <br>
                <br>
                <br>
                <br>
                <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS. ANANG
                            ZAMRONI, M.S.I.</b></u></td>
            </tr>
        </table>
    </center>

    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>
