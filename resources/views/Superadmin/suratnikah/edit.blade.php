@extends('layouts.front.backend')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
        integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('suratnikah.index') }}" style="text-decoration: none">Data Surat Nikah</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Surat Nikah</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Surat Nikah</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('suratnikah.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                                {{-- <select class="form-control  @error('kelurahan_id') is-invalid @enderror"
                                                    name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('kelurahan_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Kelurahan!</strong>
                                                    </span>
                                                @enderror --}}
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control  @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option {{ $data->dusun_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Penduduk</label>
                                                <input type="hidden" name="penduduk_id" id="penduduk"
                                                    value="{{ $data->penduduk_id }}">
                                                <input type="text"
                                                    class="form-control @error('penduduk_id') is-invalid @enderror"
                                                    id="penduduk_id" placeholder="Nama Lengkap"
                                                    value="{{ old('penduduk_id', $data->penduduk->nik . ' - ' . $data->penduduk->nama) }}">
                                                @error('penduduk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Penduduk tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Suami/Istri</label>
                                                <input type="text"
                                                    class="form-control @error('terdahulu') is-invalid @enderror"
                                                    name="terdahulu" id="terdahulu"
                                                    value="{{ old('terdahulu', $data->terdahulu) }}">
                                                @error('terdahulu')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Suami/Istri Terdahulu tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Ayah</label>
                                                <input type="hidden" name="nama_ayah" id="ayah"
                                                    value="{{ $data->nama_ayah }}">
                                                <input type="text"
                                                    class="form-control @error('nama_ayah') is-invalid @enderror"
                                                    id="father" placeholder="Nama Ayah"
                                                    value="{{ old('nama_ayah', $data->penduduk->nik . ' - ' . $data->penduduk->nama_ayah) }}"
                                                    readonly>
                                                @error('nama_ayah')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Lengkap tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Ibu</label>
                                                <input type="hidden" name="nama_ibu" id="ibu"
                                                    value="{{ $data->nama_ibu }}">
                                                <input type="text"
                                                    class="form-control @error('nama_ibu') is-invalid @enderror"
                                                    id="mother" placeholder="Nama Ibu"
                                                    value="{{ old('nama_ibu', $data->penduduk->nik . ' - ' . $data->penduduk->nama_ibu) }}"
                                                    readonly>
                                                @error('nama_ibu')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Ibu tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"
        integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>

    <script>
        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });
    </script>

    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#penduduk_id").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofillsuratnikah') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#penduduk_id').val(ui.item.label); // display the selected text
                    $('#nik').val(ui.item.value); // save selected id to input
                    $('#father').val(ui.item.ayah); // save selected id to input
                    $('#mother').val(ui.item.ibu); // save selected id to input

                    $('#penduduk').val(ui.item.id);
                    $('#niks').val(ui.item.value);
                    $('#ayah').val(ui.item.id);
                    $('#ibu').val(ui.item.id);
                    return false;
                }
            });

        });
    </script>
@endpush
