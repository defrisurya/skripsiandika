<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Cetak Laporan</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: center;
            font-size: 20px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }

        hr {
            height: 3px;
            border: none;
            border-top: 1px solid black;
        }
    </style>
</head>

<body>
    <center>
        <table width="1400">
            <tr>
                {{-- <td><img src="{{ url('/storage/logo.png') }}" width="150" height="150"></td> --}}
                <td width="2000">
                    <center>
                        <font size="4"><b>PERTUMBUHAN PENDUDUK TAHUN {{ \Carbon\Carbon::now()->isoFormat('Y') }}</b></font><br>
                        <font size="4"><b>KELAHIRAN, KEMATIAN, MASUK PENDUDUK, PINDAH PENDUDUK</b></font><br>
                        <font size="4"><b style="text-transform: uppercase">KELURAHAN
                                {{ $kelurahan->nama_kelurahan }}</b></font><br><br>
                        <font size="4" style="text-transform: uppercase"><b>BULAN
                                {{ \Carbon\Carbon::now()->isoFormat('MMMM Y') }}</b></font>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>
                </td>
            </tr>
        </table>
        {{-- <table width="1500" class="container">
            <tr>
                <td class="text2"><u><b>LAPORAN DATA KEPENDUDUKAN TAHUNAN</b></u></td>
            </tr>
        </table>
        </table>
        <table width="1500" class="container">
            <tr>
                <td width="300"><br><br></td>
                <td class="text" align="center">Kelurahan</td>
                <td width="300">: Sinduadi</td>
            </tr>
            <tr>
                <td width="300"><br><br></td>
                <td class="text" align="center">Kecamatan</td>
                <td width="300">: Mlati</td>
            </tr>
            <tr>
                <td width="300"><br><br></td>
                <td class="text" align="center">Laporan Tahun</td>
                <td width="300">: {{ date('Y', strtotime(Carbon\Carbon::now())) }}</td>
            </tr>
        </table>
        <br> --}}
        <table class="table table-bordered border-dark">
            <tr align="center">
                <th rowspan="2" valign="middle">No</th>
                <th rowspan="2" valign="middle">DUSUN</th>
                <th colspan="3">PENDUDUK AWAL BULAN INI</th>
                {{-- <th colspan="2">JUMLAH KELAHIRAN</th> --}}
                <th colspan="2">JUMLAH PENDUDUK PENDATANG</th>
                <th colspan="2">JUMLAH KEMATIAN</th>
                <th colspan="2">JUMLAH PENDUDUK PINDAH</th>
                <th colspan="3">JUMLAH PENDUDUK AKHIR BULAN</th>
            </tr>
            <tr>
                <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td>
                <td align="center"><b>J</b></td>
                {{-- <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td> --}}
                <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td>
                <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td>
                <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td>
                <td align="center"><b>L</b></td>
                <td align="center"><b>P</b></td>
                <td align="center"><b>J</b></td>
            </tr>
            @php
                $no = 1;
                $subtotal = 0;
                $subtotal1 = 0;
                $subtotal2 = 0;
                $subtotal3 = 0;
                $subtotal4 = 0;
                $subtotal5 = 0;
                $subtotal6 = 0;
                $subtotal7 = 0;
                $subtotal8 = 0;
                $subtotal9 = 0;
                $subtotal10 = 0;
                $subtotal11 = 0;
                $subtotal12 = 0;
                $subtotal13 = 0;
            @endphp
            @foreach ($dusun as $item)
                @php
                    $penduduk = App\Penduduk::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Laki-Laki')
                        ->count();
                    $penduduk1 = App\Penduduk::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Perempuan')
                        ->count();
                    $jmlpenduduk = $penduduk + $penduduk1;
                    $kelahiran = App\Kelahiran::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Laki-Laki')
                        ->count();
                    $kelahiran1 = App\Kelahiran::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Perempuan')
                        ->count();
                    $kematian = App\Kematian::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Laki-Laki')
                        ->count();
                    $kematian1 = App\Kematian::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Perempuan')
                        ->count();
                    $pindah = App\Pindah::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Laki-Laki')
                        ->count();
                    // dd($pindah);
                    $pindah1 = App\Pindah::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Perempuan')
                        ->count();
                    $pendatang = App\Pendatang::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Laki-Laki')
                        ->count();
                    $pendatang1 = App\Pendatang::where('dusun_id', $item->id)
                        ->where('jenis_kelamin', 'Perempuan')
                        ->count();
                    $total_laki = $penduduk + $kelahiran + $pendatang - $kematian - $pindah;
                    $total_perempuan = $penduduk1 + $kelahiran1 + $pendatang1 - $kematian1 - $pindah1;
                    $total = $total_laki + $total_perempuan;
                @endphp
                <tr>
                    <td align="center"><b>{{ $no++ }}</b></td>
                    <td><b>{{ $item->nama_dusun }}</b></td>
                    <td align="center"><b>{{ $penduduk }}</b></td>
                    <td align="center"><b>{{ $penduduk1 }}</b></td>
                    <td align="center"><b>{{ $jmlpenduduk }}</b></td>
                    {{-- <td align="center"><b>{{ $kelahiran }}</b></td>
                    <td align="center"><b>{{ $kelahiran1 }}</b></td> --}}
                    <td align="center"><b>{{ $pendatang }}</b></td>
                    <td align="center"><b>{{ $pendatang1 }}</b></td>
                    <td align="center"><b>{{ $kematian }}</b></td>
                    <td align="center"><b>{{ $kematian1 }}</b></td>
                    <td align="center"><b>{{ $pindah }}</b></td>
                    <td align="center"><b>{{ $pindah1 }}</b></td>
                    <td align="center"><b>{{ $total_laki }}</b></td>
                    <td align="center"><b>{{ $total_perempuan }}</b></td>
                    <td align="center"><b>{{ $total }}</b></td>
                </tr>
                @php
                    $subtotal += $penduduk;
                    $subtotal1 += $penduduk1;
                    $subtotal2 += $jmlpenduduk;
                    $subtotal3 += $kelahiran;
                    $subtotal4 += $kelahiran1;
                    $subtotal5 += $kematian;
                    $subtotal6 += $kematian1;
                    $subtotal7 += $pendatang;
                    $subtotal8 += $pendatang1;
                    $subtotal9 += $pindah;
                    $subtotal10 += $pindah1;
                    $subtotal11 += $total_laki;
                    $subtotal12 += $total_perempuan;
                    $subtotal13 += $total;
                @endphp
            @endforeach
            <tr>
                <td colspan="2" align="center"><b>Jumlah</b></td>
                <td align="center"><b>{{ $subtotal }}</b></td>
                <td align="center"><b>{{ $subtotal1 }}</b></td>
                <td align="center"><b>{{ $subtotal2 }}</b></td>
                <td align="center"><b>{{ $subtotal3 }}</b></td>
                {{-- <td align="center"><b>{{ $subtotal4 }}</b></td>
                <td align="center"><b>{{ $subtotal5 }}</b></td> --}}
                <td align="center"><b>{{ $subtotal6 }}</b></td>
                <td align="center"><b>{{ $subtotal7 }}</b></td>
                <td align="center"><b>{{ $subtotal8 }}</b></td>
                <td align="center"><b>{{ $subtotal9 }}</b></td>
                <td align="center"><b>{{ $subtotal10 }}</b></td>
                <td align="center"><b>{{ $subtotal11 }}</b></td>
                <td align="center"><b>{{ $subtotal12 }}</b></td>
                <td align="center"><b>{{ $subtotal13 }}</b></td>
            </tr>

            <br>
            <table width="750" class="container" style="float: right">
                <tr>
                    <td width="430"></td>
                    <td class="text" align="center">Yogyakarta,
                        {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
                </tr>
                <tr>
                    <td width="430"><br><br><br><br></td>
                    <br>
                    <br>
                    <br>
                    <br>
                    <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS.
                                ANANG ZAMRONI, M.S.I.</b></u></td>
                </tr>
            </table>
    </center>

    <script type="text/javascript">
        window.print();
    </script>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="{{ asset('bootstrap-4.6.0-dist/js/bootstrap.bundle.min.js') }}"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

</body>

</html>
