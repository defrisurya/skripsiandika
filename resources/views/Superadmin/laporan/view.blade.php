@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Laporan</h6>
        </nav>

        {{-- <a class="btn bg-gradient-info w-25 mt-4 mb-3" href="{{ route('pindah.create') }}"><i
                class="fas fa-plus"></i>&nbsp;
            Tambah Data Pindah</a> --}}

        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0"></div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Jenis Laporan</th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Kematian</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanKematian') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Penduduk Pindah</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanPindah') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Penduduk Pendatang</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanPendatang') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Permintaan Surat Keterangan Usaha</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanKetUsaha') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Permintaan Surat Penghantar Nikah</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanNikah') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">Laporan Keseluruhan Per Periode</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('LaporanPeriodik') }}"
                                                    class="btn bg-gradient-info" target="__blank"><i
                                                        class="fas fa-print"></i>
                                                    Print</a>
                                            </td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
