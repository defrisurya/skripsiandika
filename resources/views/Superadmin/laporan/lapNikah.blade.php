<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Cetak Laporan</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: center;
            font-size: 20px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }

        hr {
            height: 3px;
            border: none;
            border-top: 1px solid black;
        }
    </style>
</head>

<body>
    <center>
        <table width="1400">
            <tr>
                <td width="2000">
                    <center>
                        <font size="4"><b>LAPORAN DATA KEMATIAN</b></font><br>
                        <font size="4"><b style="text-transform: uppercase">KELURAHAN
                                {{ $kelurahan->nama_kelurahan }}</b></font><br><br>
                        <font size="4" style="text-transform: uppercase"><b>BULAN
                                {{ \Carbon\Carbon::now()->isoFormat('MMMM Y') }}</b></font>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>
                </td>
            </tr>
        </table>
        <table class="table table-bordered border-dark">
            <tr align="center">
                <th rowspan="2">No</th>
                <th rowspan="2">NIK</th>
                <th rowspan="2">NAMA</th>
                <th rowspan="2">NAMA AYAH</th>
                <th rowspan="2">NAMA IBU</th>
            </tr>
            <tr>
            @foreach ($nikah as $no => $item)
                @php
                    $penduduk = App\Penduduk::where('id', $item->penduduk_id)->first();
                @endphp
                <tr>
                    <td align="center"><b>{{ ++$no }}</b></td>
                    <td align="center"><b>{{ $penduduk->nik }}</b></td>
                    <td align="center"><b>{{ $penduduk->nama }}</b></td>
                    <td align="center"><b>{{ $penduduk->nama_ayah }}</b></td>
                    <td align="center"><b>{{ $penduduk->nama_ibu }}</b></td>
                </tr>
            @endforeach

            <br>
            <table width="750" class="container" style="float: right">
                <tr>
                    <td width="430"></td>
                    <td class="text" align="center">Yogyakarta,
                        {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
                </tr>
                <tr>
                    <td width="430"><br><br><br><br></td>
                    <br>
                    <br>
                    <br>
                    <br>
                    <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS.
                                ANANG ZAMRONI, M.S.I.</b></u></td>
                </tr>
            </table>
    </center>

    {{-- <script type="text/javascript">
        window.print();
    </script> --}}

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="{{ asset('bootstrap-4.6.0-dist/js/bootstrap.bundle.min.js') }}"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

</body>

</html>
