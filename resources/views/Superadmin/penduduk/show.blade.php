@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('penduduk.index') }}">Data Penduduk</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Penduduk</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Detail Data Penduduk Atas Nama : {{ $data->nama }}</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form">
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Kelurahan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Dusun</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->dusun->nama_dusun }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Nomor KK</label>
                                                <input type="text" class="form-control" value="{{ $data->kk->no_kk }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">NIK</label>
                                                <input type="number" class="form-control" value="{{ $data->nik }}"
                                                    readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama</label>
                                                <input type="text" class="form-control" value="{{ $data->nama }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Lahir</label>
                                                <input type="text" class="form-control" value="{{ $data->tempat_lahir }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tanggal Lahir</label>
                                                <input type="date" class="form-control"
                                                    value="{{ $data->tanggal_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelamin</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->jenis_kelamin }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Agama</label>
                                                <input type="text" class="form-control" value="{{ $data->agama }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Pendidikan
                                                    Terakhir</label>
                                                <input type="text" class="form-control" value="{{ $data->pendidikan }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3"><label class="form-control-label"
                                                    for="pekerjaan">Pekerjaan</label>
                                                <input type="text" class="form-control" value="{{ $data->pekerjaan }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Golongan Darah</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->golongan_darah }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Status
                                                    Perkawinan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->status_perkawinan }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Hubungan Dalam
                                                    Keluarga</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->hubungan_keluarga }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kewarganegaraan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kewarganegaraan }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">RT</label>
                                                <input type="text" class="form-control" value="{{ $data->rt }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">RW</label>
                                                <input type="text" class="form-control" value="{{ $data->rw }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama Ayah</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->nama_ayah }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama Ibu</label>
                                                <input type="text" class="form-control" value="{{ $data->nama_ibu }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="col-12">
                                            <label class="form-control-label" for="pekerjaan">Alamat</label>
                                            <textarea class="form-control" cols="30" rows="3" readonly>{{ $data->kk->alamat }}</textarea>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="col-6">
                                            <label class="form-control-label" for="pekerjaan">Status Penduduk</label>
                                            <input type="text" class="form-control"
                                                value="{{ $data->status_kependudukan }}" readonly>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
