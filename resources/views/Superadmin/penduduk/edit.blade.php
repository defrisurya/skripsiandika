@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('penduduk.index') }}">Data Penduduk</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Data Penduduk</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Data Penduduk</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('penduduk.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Kelurahan</label>
                                                <select class="form-control" name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Dusun</label>
                                                <select class="form-control" name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option {{ $data->dusun_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Nomor KK</label>
                                                <select class="form-control" name="kk_id" id="kk_id">
                                                    <option selected disabled>-- Pilih Nomor KK --</option>
                                                    @foreach ($kk as $item)
                                                        <option {{ $data->kk_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->no_kk }} -
                                                            {{ $item->nama_kepalakeluarga }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">NIK</label>
                                                <input type="number" class="form-control" id="nik" name="nik"
                                                    placeholder="Nomor Induk Keluarga" aria-label="nik"
                                                    aria-describedby="nik-addon" value="{{ old('nik', $data->nik) }}">
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama</label>
                                                <input type="text" class="form-control" name="nama" id="nama"
                                                    placeholder="Nama Penduduk" aria-label="nama"
                                                    aria-describedby="nama-addon" value="{{ old('nama', $data->nama) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lahir"
                                                    id="tempat_lahir" placeholder="Tempat Penduduk"
                                                    aria-label="tempat_lahir" aria-describedby="tempat_lahir-addon"
                                                    value="{{ old('tempat_lahir', $data->tempat_lahir) }}">
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="tanggal_lahir"
                                                    name="tanggal_lahir" placeholder="Tanggal Lahir"
                                                    aria-label="tanggal_lahir" aria-describedby="tanggal_lahir-addon"
                                                    value="{{ old('tanggal_lahir', $data->tanggal_lahir) }}">
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelamin</label>
                                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                                    <option selected disabled>-- Pilih Jenis Kelamin --</option>
                                                    <option {{ $data->jenis_kelamin == 'Laki-Laki' ? 'selected' : '' }}
                                                        value="Laki-Laki">Laki-Laki
                                                    </option>
                                                    <option {{ $data->jenis_kelamin == 'Perempuan' ? 'selected' : '' }}
                                                        value="Perempuan">Perempuan
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Agama</label>
                                                <select class="form-control" name="agama" id="agama">
                                                    <option selected disabled>-- Pilih Agama --</option>
                                                    <option {{ $data->agama == 'Islam' ? 'selected' : '' }}
                                                        value="Islam">Islam
                                                    </option>
                                                    <option {{ $data->agama == 'Kristen' ? 'selected' : '' }}
                                                        value="Kristen">Kristen
                                                    </option>
                                                    <option {{ $data->agama == 'Katholik' ? 'selected' : '' }}
                                                        value="Katholik">Katholik
                                                    </option>
                                                    <option {{ $data->agama == 'Hindu' ? 'selected' : '' }}
                                                        value="Hindu">Hindu
                                                    </option>
                                                    <option {{ $data->agama == 'Buddha' ? 'selected' : '' }}
                                                        value="Buddha">Buddha
                                                    </option>
                                                    <option {{ $data->agama == 'Khonghucu' ? 'selected' : '' }}
                                                        value="Khonghucu">Khonghucu
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Pendidikan
                                                    Terakhir</label>
                                                <select class="form-control" name="pendidikan" id="pendidikan">
                                                    <option selected disabled>-- Pilih Pendidikan --</option>
                                                    <option
                                                        {{ $data->pendidikan == 'Belum/Tidak Sekolah' ? 'selected' : '' }}
                                                        value="Belum/Tidak Sekolah">Belum/Tidak Sekolah
                                                    </option>
                                                    <option
                                                        {{ $data->pendidikan == 'Tamat SD/Sederajat' ? 'selected' : '' }}
                                                        value="Tamat SD/Sederajat">Tamat SD/Sederajat
                                                    </option>
                                                    <option {{ $data->pendidikan == 'SLTP/Sederajat' ? 'selected' : '' }}
                                                        value="SLTP/Sederajat">SLTP/Sederajat
                                                    </option>
                                                    <option {{ $data->pendidikan == 'SLTA/Sederajat' ? 'selected' : '' }}
                                                        value="SLTA/Sederajat">SLTA/Sederajat
                                                    </option>
                                                    <option {{ $data->pendidikan == 'Sarjana' ? 'selected' : '' }}
                                                        value="Sarjana">Sarjana
                                                    </option>
                                                    <option {{ $data->pendidikan == 'Diploma' ? 'selected' : '' }}
                                                        value="Diploma">Diploma
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3"><label class="form-control-label"
                                                    for="pekerjaan">Pekerjaan</label>
                                                <select class="form-control" name="pekerjaan" id="pekerjaan">
                                                    <option selected disabled>-- Pilih Pekerjaan --</option>
                                                    <option
                                                        {{ $data->pekerjaan == 'Belum/Tidak Bekerja' ? 'selected' : '' }}
                                                        value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                    </option>
                                                    <option
                                                        {{ $data->pekerjaan == 'Buruh Harian Lepas' ? 'selected' : '' }}
                                                        value="Buruh Harian Lepas">Buruh Harian Lepas
                                                    </option>
                                                    <option
                                                        {{ $data->pekerjaan == 'Mengurus Rumah Tangga' ? 'selected' : '' }}
                                                        value="Mengurus Rumah Tangga">Mengurus Rumah Tangga
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Karyawan Swasta' ? 'selected' : '' }}
                                                        value="Karyawan Swasta">Karyawan Swasta
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Karyawan BUMN' ? 'selected' : '' }}
                                                        value="Karyawan BUMN">Karyawan BUMN
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Wiraswasta' ? 'selected' : '' }}
                                                        value="Wiraswasta">Wiraswasta
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Pedagang' ? 'selected' : '' }}
                                                        value="Pedagang">Pedagang
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'PNS' ? 'selected' : '' }}
                                                        value="PNS">PNS
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Guru' ? 'selected' : '' }}
                                                        value="Guru">Guru
                                                    </option>
                                                    <option {{ $data->pekerjaan == 'Pelajar/Mahasiswa' ? 'selected' : '' }}
                                                        value="Pelajar/Mahasiswa">Pelajar/Mahasiswa
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Golongan Darah</label>
                                                <select class="form-control" name="golongan_darah" id="golongan_darah">
                                                    <option selected disabled>-- Pilih Golongan Darah --</option>
                                                    <option {{ $data->golongan_darah == 'A' ? 'selected' : '' }}
                                                        value="A">A
                                                    </option>
                                                    <option {{ $data->golongan_darah == 'B' ? 'selected' : '' }}
                                                        value="B">B
                                                    </option>
                                                    <option {{ $data->golongan_darah == 'AB' ? 'selected' : '' }}
                                                        value="AB">AB
                                                    </option>
                                                    <option {{ $data->golongan_darah == 'O' ? 'selected' : '' }}
                                                        value="O">O
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Status
                                                    Perkawinan</label>
                                                <select class="form-control" name="status_perkawinan"
                                                    id="status_perkawinan">
                                                    <option selected disabled>-- Pilih Status Perkawinan --</option>
                                                    <option
                                                        {{ $data->status_perkawinan == 'Belum Kawin' ? 'selected' : '' }}
                                                        value="Belum Kawin">Belum Kawin
                                                    </option>
                                                    <option {{ $data->status_perkawinan == 'Kawin' ? 'selected' : '' }}
                                                        value="Kawin">Kawin
                                                    </option>
                                                    <option
                                                        {{ $data->status_perkawinan == 'Cerai Mati' ? 'selected' : '' }}
                                                        value="Cerai Mati">Cerai Mati
                                                    </option>
                                                    <option
                                                        {{ $data->status_perkawinan == 'Cerai Hidup' ? 'selected' : '' }}
                                                        value="Cerai Hidup">Cerai Hidup
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Hubungan Dalam
                                                    Keluarga</label>
                                                <select class="form-control" name="hubungan_keluarga"
                                                    id="hubungan_keluarga">
                                                    <option selected disabled>-- Pilih Hubungan --</option>
                                                    <option {{ $data->hubungan_keluarga == 'Suami' ? 'selected' : '' }}
                                                        value="Suami">Suami
                                                    </option>
                                                    <option {{ $data->hubungan_keluarga == 'Istri' ? 'selected' : '' }}
                                                        value="Istri">Istri
                                                    </option>
                                                    <option {{ $data->hubungan_keluarga == 'Anak' ? 'selected' : '' }}
                                                        value="Anak">Anak
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kewarganegaraan</label>
                                                <select class="form-control" name="kewarganegaraan" id="kewarganegaraan">
                                                    <option selected disabled>-- Pilih Kewarganegaraan --</option>
                                                    <option {{ $data->kewarganegaraan == 'WNI' ? 'selected' : '' }}
                                                        value="WNI">WNI
                                                    </option>
                                                    <option {{ $data->kewarganegaraan == 'WNA' ? 'selected' : '' }}
                                                        value="WNA">WNA
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Nama Ayah</label>
                                                <input type="text" class="form-control" name="nama_ayah"
                                                    id="nama_ayah" placeholder="Nama Ayah" aria-label="nama_ayah"
                                                    aria-describedby="nama_ayah-addon"
                                                    value="{{ old('nama_ayah', $data->nama_ayah) }}">
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Nama Ibu</label>
                                                <input type="text" class="form-control" id="nama_ibu"
                                                    name="nama_ibu" placeholder="Nama Ibu" aria-label="nama_ibu"
                                                    aria-describedby="nama_ibu-addon"
                                                    value="{{ old('nama_ibu', $data->nama_ibu) }}">
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">RT</label>
                                                <input type="text" class="form-control" name="rt" id="rt"
                                                    placeholder="rt Penduduk" aria-label="rt" aria-describedby="rt-addon"
                                                    value="{{ old('rt', $data->rt) }}">
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">RW</label>
                                                <input type="text" class="form-control" id="rw" name="rw"
                                                    placeholder="Nomor Induk Keluarga" aria-label="rw"
                                                    aria-describedby="rw-addon" value="{{ old('rw', $data->rw) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
