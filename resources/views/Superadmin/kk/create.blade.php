@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kk.index') }}">Data Kartu Keluarga</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Kartu Keluarga</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Tambah Data Kartu Keluarga</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('kk.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                {{-- <select class="form-control @error('kelurahan_id') is-invalid @enderror"
                                                    name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('kelurahan_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Kelurahan!</strong>
                                                    </span>
                                                @enderror --}}
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nomor Kartu Keluarga</label>
                                                <input type="number"
                                                    class="form-control @error('no_kk') is-invalid @enderror" name="no_kk"
                                                    id="no_kk" placeholder="Nomor Kartu Keluarga" aria-label="no_kk"
                                                    aria-describedby="no_kk-addon" value="{{ old('no_kk') }}">
                                                @error('no_kk')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nomor Kartu Keluarga tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Kepala Keluarga</label>
                                                <input type="text"
                                                    class="form-control @error('nama_kepalakeluarga') is-invalid @enderror"
                                                    name="nama_kepalakeluarga" id="nama_kepalakeluarga"
                                                    placeholder="Nama Kepala Keluarga" aria-label="nama_kepalakeluarga"
                                                    aria-describedby="nama_kepalakeluarga-addon"
                                                    value="{{ old('nama_kepalakeluarga') }}">
                                                @error('nama_kepalakeluarga')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Kepala Keluarga tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-control-label" for="alamat">Alamat</label>
                                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" style="color: black"
                                            placeholder="Alamat">{{ old('alamat') }}</textarea>
                                        @error('alamat')
                                            <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <hr class="my-2 mt-5">
                                    <h6 class="text-center">Tambah Data Anggota Keluarga</h6>
                                    <hr class="my-2 mb-2">
                                    <div class="text-dark after-add-more">
                                        <div class="row">
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="nik">NIK</label>
                                                <input type="number"
                                                    class="form-control @error('nik') is-invalid @enderror" name="nik[]"
                                                    style="color: black" placeholder="NIK" value="{{ old('nik') }}">
                                                @error('nik')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="nama">Nama</label>
                                                <input type="text"
                                                    class="form-control @error('nama') is-invalid @enderror" name="nama[]"
                                                    style="color: black" placeholder="Nama" value="{{ old('nama') }}">
                                                @error('nama')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="tempat_lahir">Tempat Lahir</label>
                                                <input type="text"
                                                    class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                    name="tempat_lahir[]" style="color: black" placeholder="Tempat Lahir"
                                                    value="{{ old('tempat_lahir') }}">
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="tanggal_lahir">Tanggal
                                                    Lahir</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                    name="tanggal_lahir[]" style="color: black"
                                                    value="{{ old('tanggal_lahir') }}">
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="jenis_kelamin">Jenis
                                                    Kelamin</label>
                                                <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    name="jenis_kelamin[]" style="color: black">
                                                    <option selected disabled>Jenis Kelamin</option>
                                                    <option value="Laki-Laki" {{ old('jenis_kelamin') }}>Laki-laki
                                                    </option>
                                                    <option value="Perempuan" {{ old('jenis_kelamin') }}>Perempuan
                                                    </option>
                                                </select>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="agama">Agama</label>
                                                <select class="form-control @error('agama') is-invalid @enderror"
                                                    name="agama[]" style="color: black">
                                                    <option selected disabled>Agama</option>
                                                    <option value="Islam" {{ old('agama') }}>Islam</option>
                                                    <option value="Kristen" {{ old('agama') }}>Kristen</option>
                                                    <option value="Katholik" {{ old('agama') }}>Katholik</option>
                                                    <option value="Hindu" {{ old('agama') }}>Hindu</option>
                                                    <option value="Buddha" {{ old('agama') }}>Buddha</option>
                                                    <option value="Khonghucu" {{ old('agama') }}>Khonghucu</option>
                                                </select>
                                                @error('agama')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="pendidikan">Pendidikan
                                                    Terakhir</label>
                                                <select class="form-control @error('pendidikan') is-invalid @enderror"
                                                    name="pendidikan[]" style="color: black">
                                                    <option selected disabled>Pendidikan</option>
                                                    <option value="Belum/Tidak Sekolah" {{ old('pendidikan') }}>
                                                        Belum/Tidak
                                                        Sekolah</option>
                                                    <option value="Tamat SD/Sederajat" {{ old('pendidikan') }}>Tamat
                                                        SD/Sederajat
                                                    </option>
                                                    <option value="SLTP/Sederajat" {{ old('pendidikan') }}>
                                                        SLTP/Sederajat
                                                    </option>
                                                    <option value="SLTA/Sederajat" {{ old('pendidikan') }}>
                                                        SLTA/Sederajat
                                                    </option>
                                                    <option value="Sarjana" {{ old('pendidikan') }}>Sarjana</option>
                                                    <option value="Diploma" {{ old('pendidikan') }}>Diploma</option>
                                                </select>
                                                @error('pendidikan')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="pekerjaan">Pekerjaan</label>
                                                <select style="color: black"
                                                    class="form-control @error('pekerjaan') is-invalid @enderror"
                                                    name="pekerjaan[]">
                                                    <option selected disabled>Pekerjaan</option>
                                                    <option value="Belum/Tidak Bekerja" {{ old('pekerjaan') }}>
                                                        Belum/Tidak
                                                        Bekerja</option>
                                                    <option value="Buruh Harian Lepas" {{ old('pekerjaan') }}>Buruh
                                                        Harian Lepas
                                                    </option>
                                                    <option value="Mengurus Rumah Tangga" {{ old('pekerjaan') }}>
                                                        Mengurus
                                                        Rumah
                                                        Tangga</option>
                                                    <option value="Karyawan Swasta" {{ old('pekerjaan') }}>Karyawan
                                                        Swasta
                                                    </option>
                                                    <option value="Karyawan BUMN" {{ old('pekerjaan') }}>Karyawan BUMN
                                                    </option>
                                                    <option value="Wiraswasta" {{ old('pekerjaan') }}>Wiraswasta
                                                    </option>
                                                    <option value="Pedagang" {{ old('pekerjaan') }}>Pedagang</option>
                                                    <option value="PNS" {{ old('pekerjaan') }}>PNS</option>
                                                    <option value="Guru" {{ old('pekerjaan') }}>Guru</option>
                                                    <option value="Pelajar/Mahasiswa" {{ old('pekerjaan') }}>
                                                        Pelajar/Mahasiswa
                                                    </option>
                                                </select>
                                                @error('pekerjaan')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="status_perkawinan">Status
                                                    Perkawinan</label>
                                                <select
                                                    class="form-control @error('status_perkawinan') is-invalid @enderror"
                                                    name="status_perkawinan[]" style="color: black">
                                                    <option selected disabled>Status Perkawinan</option>
                                                    <option value="Belum Kawin" {{ old('status_perkawinan') }}>Belum
                                                        Kawin
                                                    </option>
                                                    <option value="Kawin" {{ old('status_perkawinan') }}>Kawin</option>
                                                    <option value="Cerai Mati" {{ old('status_perkawinan') }}>Cerai Mati
                                                    </option>
                                                    <option value="Cerai Hidup" {{ old('status_perkawinan') }}>Cerai
                                                        Hidup
                                                    </option>
                                                </select>
                                                @error('status_perkawinan')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="golongan_darah">Golongan
                                                    Darah</label>
                                                <select class="form-control @error('golongan_darah') is-invalid @enderror"
                                                    name="golongan_darah[]" style="color: black">
                                                    <option selected disabled>Golongan Darah</option>
                                                    <option value="A" {{ old('golongan_darah') }}>A</option>
                                                    <option value="B" {{ old('golongan_darah') }}>B</option>
                                                    <option value="AB" {{ old('golongan_darah') }}>AB</option>
                                                    <option value="O" {{ old('golongan_darah') }}>O</option>
                                                </select>
                                                @error('golongan_darah')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label" for="hubungan_keluarga">Status Hubungan
                                                    Dalam
                                                    Keluarga</label>
                                                <select
                                                    class="form-control @error('hubungan_keluarga') is-invalid @enderror"
                                                    name="hubungan_keluarga[]" style="color: black">
                                                    <option selected disabled>Status Hubungan Dalam Keluarga</option>
                                                    <option value="Suami" {{ old('hubungan_keluarga') }}>Suami</option>
                                                    <option value="Istri" {{ old('hubungan_keluarga') }}>Istri</option>
                                                    <option value="Anak" {{ old('hubungan_keluarga') }}>Anak</option>
                                                </select>
                                                @error('hubungan_keluarga')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-4 col-md-6">
                                                <label class="form-control-label"
                                                    for="kewarganegaraan">Kewarganegaraan</label>
                                                <select
                                                    class="form-control @error('kewarganegaraan') is-invalid @enderror"
                                                    name="kewarganegaraan[]" style="color: black">
                                                    <option selected disabled>Kewarganegaraan</option>
                                                    <option value="WNI" {{ old('kewarganegaraan') }}>WNI</option>
                                                    <option value="WNA" {{ old('kewarganegaraan') }}>WNA</option>
                                                </select>
                                                @error('kewarganegaraan')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label class="form-control-label" for="nama_ayah">Nama Ayah</label>
                                                <input type="text"
                                                    class="form-control @error('nama_ayah') is-invalid @enderror"
                                                    name="nama_ayah[]" style="color: black" placeholder="Nama Ayah"
                                                    value="{{ old('nama_ayah') }}">
                                                @error('nama_ayah')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label class="form-control-label" for="nama_ibu">Nama Ibu</label>
                                                <input type="text"
                                                    class="form-control @error('nama_ibu') is-invalid @enderror"
                                                    name="nama_ibu[]" style="color: black" placeholder="Nama Ibu"
                                                    value="{{ old('nama_ibu') }}">
                                                @error('nama_ibu')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label class="form-control-label" for="rt">RT</label>
                                                <input type="text"
                                                    class="form-control @error('rt') is-invalid @enderror" name="rt[]"
                                                    style="color: black" placeholder="RT" value="{{ old('rt') }}">
                                                @error('rt')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6">
                                                <label class="form-control-label" for="rw">RW</label>
                                                <input type="text"
                                                    class="form-control @error('rw') is-invalid @enderror" name="rw[]"
                                                    style="color: black" placeholder="RW" value="{{ old('rw') }}">
                                                @error('rw')
                                                    <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <a class="btn btn-success add-more" style="float: inline"><i
                                                class="fas fa-plus"></i>
                                            Tambah Kolom Anggota Keluarga</a>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="simpan"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                            <div class="copy invisible" hidden>
                                <div class="text-dark">
                                    <hr class="my-2">
                                    <label class="form-control-label" for="nik">Anggota Keluarga</label>
                                    <div class="row">
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="nik">NIK</label>
                                            <input type="number" class="form-control @error('nik') is-invalid @enderror"
                                                name="nik[]" style="color: black" placeholder="NIK"
                                                value="{{ old('nik') }}">
                                            @error('nik')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="nama">Nama</label>
                                            <input type="text"
                                                class="form-control @error('nama') is-invalid @enderror" name="nama[]"
                                                style="color: black" placeholder="Nama" value="{{ old('nama') }}">
                                            @error('nama')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="tempat_lahir">Tempat Lahir</label>
                                            <input type="text"
                                                class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                name="tempat_lahir[]" style="color: black" placeholder="Tempat Lahir"
                                                value="{{ old('tempat_lahir') }}">
                                            @error('tempat_lahir')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="tanggal_lahir">Tanggal Lahir</label>
                                            <input type="date"
                                                class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                name="tanggal_lahir[]" style="color: black"
                                                value="{{ old('tanggal_lahir') }}">
                                            @error('tanggal_lahir')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="jenis_kelamin">Jenis Kelamin</label>
                                            <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                name="jenis_kelamin[]" style="color: black">
                                                <option selected disabled>Jenis Kelamin</option>
                                                <option value="Laki-Laki" {{ old('jenis_kelamin') }}>Laki-laki</option>
                                                <option value="Perempuan" {{ old('jenis_kelamin') }}>Perempuan</option>
                                            </select>
                                            @error('jenis_kelamin')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="agama">Agama</label>
                                            <select class="form-control @error('agama') is-invalid @enderror"
                                                name="agama[]" style="color: black">
                                                <option selected disabled>Agama</option>
                                                <option value="Islam" {{ old('agama') }}>Islam</option>
                                                <option value="Kristen" {{ old('agama') }}>Kristen</option>
                                                <option value="Katholik" {{ old('agama') }}>Katholik</option>
                                                <option value="Hindu" {{ old('agama') }}>Hindu</option>
                                                <option value="Buddha" {{ old('agama') }}>Buddha</option>
                                                <option value="Khonghucu" {{ old('agama') }}>Khonghucu</option>
                                            </select>
                                            @error('agama')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="pendidikan">Pendidikan Terakhir</label>
                                            <select class="form-control @error('pendidikan') is-invalid @enderror"
                                                name="pendidikan[]" style="color: black">
                                                <option selected disabled>Pendidikan</option>
                                                <option value="Belum/Tidak Sekolah" {{ old('pendidikan') }}>Belum/Tidak
                                                    Sekolah</option>
                                                <option value="Tamat SD/Sederajat" {{ old('pendidikan') }}>Tamat
                                                    SD/Sederajat</option>
                                                <option value="SLTP/Sederajat" {{ old('pendidikan') }}>SLTP/Sederajat
                                                </option>
                                                <option value="SLTA/Sederajat" {{ old('pendidikan') }}>SLTA/Sederajat
                                                </option>
                                                <option value="Sarjana" {{ old('pendidikan') }}>Sarjana</option>
                                                <option value="Diploma" {{ old('pendidikan') }}>Diploma</option>
                                            </select>
                                            @error('pendidikan')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="pekerjaan">Pekerjaan</label>
                                            <select style="color: black"
                                                class="form-control @error('pekerjaan') is-invalid @enderror"
                                                name="pekerjaan[]">
                                                <option selected disabled>Pekerjaan</option>
                                                <option value="Belum/Tidak Bekerja" {{ old('pekerjaan') }}>
                                                    Belum/Tidak
                                                    Bekerja</option>
                                                <option value="Buruh Harian Lepas" {{ old('pekerjaan') }}>Buruh
                                                    Harian Lepas
                                                </option>
                                                <option value="Mengurus Rumah Tangga" {{ old('pekerjaan') }}>
                                                    Mengurus
                                                    Rumah
                                                    Tangga</option>
                                                <option value="Karyawan Swasta" {{ old('pekerjaan') }}>Karyawan
                                                    Swasta
                                                </option>
                                                <option value="Karyawan BUMN" {{ old('pekerjaan') }}>Karyawan BUMN
                                                </option>
                                                <option value="Wiraswasta" {{ old('pekerjaan') }}>Wiraswasta
                                                </option>
                                                <option value="Pedagang" {{ old('pekerjaan') }}>Pedagang</option>
                                                <option value="PNS" {{ old('pekerjaan') }}>PNS</option>
                                                <option value="Guru" {{ old('pekerjaan') }}>Guru</option>
                                                <option value="Pelajar/Mahasiswa" {{ old('pekerjaan') }}>
                                                    Pelajar/Mahasiswa
                                                </option>
                                            </select>
                                            @error('pekerjaan')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="status_perkawinan">Status
                                                Perkawinan</label>
                                            <select class="form-control @error('status_perkawinan') is-invalid @enderror"
                                                name="status_perkawinan[]" style="color: black">
                                                <option selected disabled>Status Perkawinan</option>
                                                <option value="Belum Kawin" {{ old('status_perkawinan') }}>Belum Kawin
                                                </option>
                                                <option value="Kawin" {{ old('status_perkawinan') }}>Kawin</option>
                                                <option value="Cerai Mati" {{ old('status_perkawinan') }}>Cerai Mati
                                                </option>
                                                <option value="Cerai Hidup" {{ old('status_perkawinan') }}>Cerai Hidup
                                                </option>
                                            </select>
                                            @error('status_perkawinan')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="golongan_darah">Golongan Darah</label>
                                            <select class="form-control @error('golongan_darah') is-invalid @enderror"
                                                name="golongan_darah[]" style="color: black">
                                                <option selected disabled>Golongan Darah</option>
                                                <option value="A" {{ old('golongan_darah') }}>A</option>
                                                <option value="B" {{ old('golongan_darah') }}>B</option>
                                                <option value="AB" {{ old('golongan_darah') }}>AB</option>
                                                <option value="O" {{ old('golongan_darah') }}>O</option>
                                            </select>
                                            @error('golongan_darah')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label" for="hubungan_keluarga">Status Hubungan
                                                Dalam
                                                Keluarga</label>
                                            <select class="form-control @error('hubungan_keluarga') is-invalid @enderror"
                                                name="hubungan_keluarga[]" style="color: black">
                                                <option selected disabled>Status Hubungan Dalam Keluarga</option>
                                                <option value="Suami" {{ old('hubungan_keluarga') }}>Suami</option>
                                                <option value="Istri" {{ old('hubungan_keluarga') }}>Istri</option>
                                                <option value="Anak" {{ old('hubungan_keluarga') }}>Anak</option>
                                            </select>
                                            @error('hubungan_keluarga')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-4 col-md-6">
                                            <label class="form-control-label"
                                                for="kewarganegaraan">Kewarganegaraan</label>
                                            <select class="form-control @error('kewarganegaraan') is-invalid @enderror"
                                                name="kewarganegaraan[]" style="color: black">
                                                <option selected disabled>Kewarganegaraan</option>
                                                <option value="WNI" {{ old('kewarganegaraan') }}>WNI</option>
                                                <option value="WNA" {{ old('kewarganegaraan') }}>WNA</option>
                                            </select>
                                            @error('kewarganegaraan')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        {{-- <div class="form-group col-12">
                                            <label class="form-control-label" for="alamat">Alamat</label>
                                            <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat[]" style="color: black"
                                                placeholder="Alamat">{{ old('alamat') }}</textarea>
                                            @error('alamat')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div> --}}
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label class="form-control-label" for="nama_ayah">Nama Ayah</label>
                                            <input type="text"
                                                class="form-control @error('nama_ayah') is-invalid @enderror"
                                                name="nama_ayah[]" style="color: black" placeholder="Nama Ayah"
                                                value="{{ old('nama_ayah') }}">
                                            @error('nama_ayah')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label class="form-control-label" for="nama_ibu">Nama Ibu</label>
                                            <input type="text"
                                                class="form-control @error('nama_ibu') is-invalid @enderror"
                                                name="nama_ibu[]" style="color: black" placeholder="Nama Ibu"
                                                value="{{ old('nama_ibu') }}">
                                            @error('nama_ibu')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label class="form-control-label" for="rt">RT</label>
                                            <input type="text" class="form-control @error('rt') is-invalid @enderror"
                                                name="rt[]" style="color: black" placeholder="RT"
                                                value="{{ old('rt') }}">
                                            @error('rt')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label class="form-control-label" for="rw">RW</label>
                                            <input type="text" class="form-control @error('rw') is-invalid @enderror"
                                                name="rw[]" style="color: black" placeholder="RW"
                                                value="{{ old('rw') }}">
                                            @error('rw')
                                                <span class="invalid-feedback font-weight-bold">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <a class="btn btn-danger remove" style="float: inline"><i class="fas fa-trash"></i>
                                        Hapus Kolom</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".add-more").click(function() {
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });
            // saat tombol remove dklik control group akan dihapus
            $("body").on("click", ".remove", function() {
                $(this).parents(".text-dark").remove();
            });
        });
    </script>
@endpush
