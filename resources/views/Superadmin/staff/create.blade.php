@extends('layouts.front.backend')

@section('css')
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('staff.index') }}" style="text-decoration: none">Data Staff</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Staff</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Tambah Data Staff</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('staff.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <select class="form-control @error('kelurahan_id') is-invalid @enderror"
                                                    name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('kelurahan_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Kelurahan!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <select class="form-control @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <input type="number" class="form-control @error('nik') is-invalid @enderror"
                                                    name="nik" id="nik" placeholder="NIK" aria-label="nik"
                                                    aria-describedby="nik-addon" value="{{ old('nik') }}">
                                                @error('nik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>NIK tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <input type="number" class="form-control @error('nip') is-invalid @enderror"
                                                    name="nip" id="nip" placeholder="NIP" aria-label="nip"
                                                    aria-describedby="nip-addon" value="{{ old('nip') }}">
                                                @error('nip')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>NIP tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                    name="nama" id="nama" placeholder="Nama" aria-label="nama"
                                                    aria-describedby="nama-addon" value="{{ old('nama') }}">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <input type="number"
                                                    class="form-control @error('no_hp') is-invalid @enderror" name="no_hp"
                                                    id="no_hp" placeholder="Nomor Telepon" aria-label="no_hp"
                                                    aria-describedby="no_hp-addon" value="{{ old('no_hp') }}">
                                                @error('no_hp')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nomor Telepon tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                                    name="email" id="email" placeholder="Email" aria-label="email"
                                                    aria-describedby="email-addon" value="{{ old('email') }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Email tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <input type="text"
                                                    class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                    name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir"
                                                    aria-label="tempat_lahir" aria-describedby="tempat_lahir-addon"
                                                    value="{{ old('tempat_lahir') }}">
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <input type="date"
                                                    class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                    name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir"
                                                    aria-label="tanggal_lahir" aria-describedby="tanggal_lahir-addon"
                                                    value="{{ old('tanggal_lahir') }}">
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    name="jenis_kelamin" style="color: black">
                                                    <option selected disabled>Jenis Kelamin</option>
                                                    <option value="Laki-Laki" {{ old('jenis_kelamin') }}>Laki-laki
                                                    </option>
                                                    <option value="Perempuan" {{ old('jenis_kelamin') }}>Perempuan
                                                    </option>
                                                </select>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Jenis Kelamin!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <input type="text"
                                                    class="form-control @error('jabatan') is-invalid @enderror"
                                                    name="jabatan" id="jabatan" placeholder="Jabatan" aria-label="jabatan"
                                                    aria-describedby="jabatan-addon" value="{{ old('jabatan') }}">
                                                @error('jabatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jabatan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <textarea name="alamat" id="alamat" class="form-control w-100 @error('alamat') is-invalid @enderror" rows="4"
                                            placeholder="Alamat">{{ old('alamat') }}</textarea>
                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="simpan"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#kecamatan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#kelurahan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });

        $('#penduduk_id').select2({
            theme: 'bootstrap-5'
        });
    </script>
@endpush
