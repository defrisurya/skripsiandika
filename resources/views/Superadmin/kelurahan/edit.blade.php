@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kelurahan.index') }}">Data Kelurahan</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Data Kelurahan</h6>
        </nav>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-6">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Edit Kelurahan</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('kelurahan.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')>
                                    <input type="text" class="form-control" name="nama_kelurahan" id="nama_kelurahan"
                                        placeholder="Nama kelurahan" aria-label="nama_kelurahan"
                                        aria-describedby="nama_kelurahan-addon"
                                        value="{{ old('nama_kelurahan', $data->nama_kelurahan) }}">
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
@endsection
