@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Data Kelurahan</h6>
        </nav>
        <div class="row">
            <div class="col-5">
                <div class="card mb-4">
                    <div class="card-header pb-0"></div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Nama Kelurahan</th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $item)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $item->nama_kelurahan }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('kelurahan.edit', $item->id) }}"
                                                    class="dropdown-item"><i class="fas fa-edit"></i></a>
                                                <form action="{{ route('kelurahan.destroy', $item->id) }}" method="POST"
                                                    onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                    {!! method_field('delete') . csrf_field() !!}
                                                    <button class="dropdown-item" type="submit">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2" class="text-center">
                                                <h6 class="text-secondary"><i>Data Masih Kosong!</i></h6>
                                            </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-7">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Tambah Kelurahan</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('kelurahan.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <input type="text"
                                            class="form-control @error('nama_kelurahan') is-invalid @enderror"
                                            name="nama_kelurahan" id="nama_kelurahan" placeholder="Nama Kelurahan"
                                            aria-label="nama_kelurahan" aria-describedby="nama_kelurahan-addon"
                                            value="{{ old('nama_kelurahan') }}">
                                        @error('nama_kelurahan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Nama Kelurahan tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
