<!DOCTYPE html>
<html>

<head>
    <title>Cetak Surat Pengantar Akta Kematian</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: center;
            font-size: 15px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }
    </style>
</head>

<body>
    <center>
        <table width="750">
            <tr>
                <td><img src="{{ asset('front') }}/home/img/kop_surat.png" width="100%" height="160"></td>
            </tr>
            <table width="750">
                <tr>
                    <td class="text2" style="text-transform: uppercase"><u><b>Surat Pengantar Akta Kematian</b></u></td>
                </tr>
            </table>
        </table>
        <table width="750">
            <tr class="text2">
                <td>Nomor</td>
                <td width="750">: -</td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Nama Kepala Keluarga : {{ $data->kk->nama_kepalakeluarga }}</font>
                </td>
            </tr>
            <tr>
                <td>
                    <font size="3">Nomor Kartu Keluarga : {{ $data->kk->no_kk }}</font>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>JENAZAH</b></font>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $data->penduduk->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $data->penduduk->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Jenis Kelamin</td>
                <td width="525">: <b>{{ $data->penduduk->jenis_kelamin }}</b></td>
            </tr>
            <tr>
                <td>4. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $data->penduduk->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($data->penduduk->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>5. Agama</td>
                <td width="525">: <b>{{ $data->penduduk->agama }}</b></td>
            </tr>
            <tr>
                <td>6. Pekerjaan</td>
                <td width="525">: <b>{{ $data->penduduk->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>7. Alamat</td>
                <td width="525">: <b>{{ $data->kk->alamat }}</b></td>
            </tr>
            <tr>
                <td>8. Tanggal Kematian</td>
                <td width="525">: <b>{{ Carbon\Carbon::parse($data->tanggal_kematian)->translatedFormat('l') }},
                        {{ date('d/m/Y', strtotime($data->tanggal_kematian)) }}</b></td>
            </tr>
            <tr>
                <td>9. Pukul</td>
                <td width="525">: <b>{{ $data->jam_kematian . ' WIB' }}</b></td>
            </tr>
            <tr>
                <td>10. Sebab Kematian</td>
                <td width="525">: <b>{{ $data->penyebab_kematian }}</b></td>
            </tr>
            <tr>
                <td>11. Tempat Kematian</td>
                <td width="525">: <b>{{ $data->tempat_kematian }}</b></td>
            </tr>
            <tr>
                <td>12. Yang Menerangkan</td>
                <td width="525">: <b>{{ $data->menerangkan }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>AYAH</b></font>
                </td>
            </tr>
        </table>
        @php
            $ayah = App\Penduduk::where('id', $data->ayah)->first();
        @endphp
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $ayah->nama_ayah }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $ayah->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $ayah->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($ayah->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>4. Pekerjaan</td>
                <td width="525">: <b>{{ $ayah->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>5. Alamat</td>
                <td width="525">: <b>{{ $ayah->kk->alamat }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>IBU</b></font>
                </td>
            </tr>
        </table>
        @php
            $ibu = App\Penduduk::where('id', $data->ibu)->first();
        @endphp
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $ibu->nama_ibu }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $ibu->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $ibu->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($ibu->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>4. Pekerjaan</td>
                <td width="525">: <b>{{ $ibu->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>5. Alamat</td>
                <td width="525">: <b>{{ $ibu->kk->alamat }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>PELAPOR</b></font>
                </td>
            </tr>
        </table>
        @php
            $pelapor = App\Penduduk::where('id', $data->pelapor)->first();
            $alamatpelapor = App\KK::where('id', $pelapor->kk_id)->first();
            // dd($alamatpelapor->alamat);
        @endphp
        <table width="750">
            <tr class="text2">
                <td>1. Tanggal Pelaporan</td>
                <td width="525">: <b> {{ date('d/m/Y', strtotime($data->tanggal_pelaporan)) }}</b></td>
            </tr>
            <tr class="text2">
                <td>2. Nama Lengkap</td>
                <td width="525">: <b>{{ $pelapor->nama }}</b></td>
            </tr>
            <tr>
                <td>3. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $pelapor->nik }}</b></td>
            </tr>
            <tr>
                <td>4. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $pelapor->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($pelapor->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>5. Pekerjaan</td>
                <td width="525">: <b>{{ $pelapor->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>6. Alamat</td>
                <td width="525">: <b>{{ $alamatpelapor->alamat }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>SAKSI 1</b></font>
                </td>
            </tr>
        </table>
        @php
            $saksi1 = App\Penduduk::where('id', $data->saksi1)->first();
            $alamatsaksi1 = App\KK::where('id', $saksi1->kk_id)->first();
            // dd($alamatsaksi1->alamat);
        @endphp
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $saksi1->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $saksi1->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $saksi1->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($saksi1->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>4. Pekerjaan</td>
                <td width="525">: <b>{{ $saksi1->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>5. Alamat</td>
                <td width="525">: <b>{{ $alamatsaksi1->alamat }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr>
                <td>
                    <font size="3"><b>SAKSI 2</b></font>
                </td>
            </tr>
        </table>
        @php
            $saksi2 = App\Penduduk::where('id', $data->saksi2)->first();
            $alamatsaksi2 = App\KK::where('id', $saksi2->kk_id)->first();
            // dd($alamatsaksi2->alamat);
        @endphp
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $saksi2->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $saksi2->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $saksi2->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($saksi2->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>4. Pekerjaan</td>
                <td width="525">: <b>{{ $saksi2->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>5. Alamat</td>
                <td width="525">: <b>{{ $alamatsaksi2->alamat }}</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        {{-- <table width="750">
            <tr>
                <td>
                    <font size="3">Demikian surat Pengantar ini dibuat dengan mengingat sumpah jabatan dan untuk
                        dipergunakan sebagaimana
                        mestinya.
                    </font>
                </td>
            </tr>
        </table> --}}
        <table width="750">
            <tr>
                <td width="430"></td>
                <td class="text" align="center">Yogyakarta,
                    {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
            </tr>
            <tr>
                <td width="430"><br><br><br><br></td>
                <br>
                <br>
                <br>
                <br>
                <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS. ANANG
                            ZAMRONI, M.S.I.</b></u></td>
            </tr>
        </table>
    </center>

    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>
