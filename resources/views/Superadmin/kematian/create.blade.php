@extends('layouts.front.backend')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
        integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kematian.index') }}" style="text-decoration: none">Data Kematian</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Kematian</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Tambah Data Kematian</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <div class="mb-3">
                                    <div class="row g-3">
                                        <div class="col-4">
                                            <label for="">Kode Permintaan Surat</label>
                                            <input type="text" class="form-control" id="request_code">
                                        </div>
                                    </div>
                                </div>
                                <form role="form" method="POST" action="{{ route('kematian.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="hidden" name="jenis_kelamin" id="jk">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Penduduk</label>
                                                <input type="hidden" name="penduduk_id" id="penduduk">
                                                <input type="text"
                                                    class="form-control @error('penduduk_id') is-invalid @enderror"
                                                    id="penduduk_id">
                                                @error('penduduk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Penduduk!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Nomor Kartu Keluarga</label>
                                                <input type="hidden" name="kk_id" id="kk">
                                                <input type="text"
                                                    class="form-control @error('kk_id') is-invalid @enderror" id="kk_id"
                                                    readonly>
                                                @error('kk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Nomor Kartu Keluarga!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Ayah</label>
                                                <input type="hidden" name="ayah" id="ayah">
                                                <input type="text"
                                                    class="form-control @error('ayah') is-invalid @enderror" id="nama_ayah"
                                                    readonly>
                                                @error('ayah')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Penduduk!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Ibu</label>
                                                <input type="hidden" name="ibu" id="ibu">
                                                <input type="text"
                                                    class="form-control @error('ibu') is-invalid @enderror" id="nama_ibu"
                                                    readonly>
                                                @error('ibu')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Ibu!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Tanggal Pelaporan</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_pelaporan') is-invalid @enderror"
                                                    name="tanggal_pelaporan" id="tgl_pelaporan"
                                                    placeholder="Tanggal Pelaporan" aria-label="tanggal_pelaporan"
                                                    aria-describedby="tanggal_pelaporan-addon"
                                                    value="{{ old('tanggal_pelaporan') }}">
                                                @error('tanggal_pelaporan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Pelaporan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Pelapor</label>
                                                <select class="form-control @error('pelapor') is-invalid @enderror"
                                                    name="pelapor" id="pelapor">
                                                    <option selected disabled>-- Pilih Pelapor --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nik }} -
                                                            {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('pelapor')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Pelapor!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Saksi 1</label>
                                                <select class="form-control @error('saksi1') is-invalid @enderror"
                                                    name="saksi1" id="saksi1">
                                                    <option selected disabled>-- Pilih Saksi 1 --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nik }} -
                                                            {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('saksi1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Nama Saksi 1!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Saksi 2</label>
                                                <select class="form-control @error('saksi2') is-invalid @enderror"
                                                    name="saksi2" id="saksi2">
                                                    <option selected disabled>-- Pilih Saksi 2 --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nik }} -
                                                            {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('saksi2')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Saksi 2!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Tanggal Meninggal</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_kematian') is-invalid @enderror"
                                                    name="tanggal_kematian" id="tgl_meninggal"
                                                    placeholder="Tanggal Meninggal" aria-label="tanggal_kematian"
                                                    aria-describedby="tanggal_kematian-addon"
                                                    value="{{ old('tanggal_kematian') }}">
                                                @error('tanggal_kematian')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Meninggal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Tempat Meninggal</label>
                                                <input type="text"
                                                    class="form-control @error('tempat_kematian') is-invalid @enderror"
                                                    name="tempat_kematian" id="tmpt_meninggal"
                                                    placeholder="Tempat Meninggal" aria-label="tempat_kematian"
                                                    aria-describedby="tempat_kematian-addon"
                                                    value="{{ old('tempat_kematian') }}">
                                                @error('tempat_kematian')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Meninggal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Jam Meninggal</label>
                                                <input type="time"
                                                    class="form-control @error('jam_kematian') is-invalid @enderror"
                                                    name="jam_kematian" id="jam_meninggal" placeholder="Jam Meninggal"
                                                    aria-label="jam_kematian" aria-describedby="jam_kematian-addon"
                                                    value="{{ old('jam_kematian') }}">
                                                @error('jam_kematian')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jam Meninggal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Penyebab Meninggal</label>
                                        <textarea name="penyebab_kematian" id="penyebab_meninggal"
                                            class="form-control w-100 @error('penyebab_kematian') is-invalid @enderror" rows="4"
                                            placeholder="Penyebab Meninggal">{{ old('penyebab_kematian') }}</textarea>
                                        @error('penyebab_kematian')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Penyebab Meninggal tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Menerangkan</label>
                                        <textarea name="menerangkan" id="menerangkan" class="form-control w-100 @error('menerangkan') is-invalid @enderror"
                                            rows="4" placeholder="Yang Menerangkan">{{ old('menerangkan') }}</textarea>
                                        @error('menerangkan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Yang Menerangkan tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="simpan"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"
        integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>

    <script>
        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });
        $('#pelapor').select2({
            theme: 'bootstrap-5'
        });
        $('#saksi1').select2({
            theme: 'bootstrap-5'
        });
        $('#saksi2').select2({
            theme: 'bootstrap-5'
        });
    </script>

    {{-- Autofill From Penduduk ID --}}
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#penduduk_id").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofill') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#penduduk_id').val(ui.item.label); // display the selected text
                    $('#kk_id').val(ui.item.value); // save selected id to input
                    $('#nama_ayah').val(ui.item.ayah); // save selected id to input
                    $('#nama_ibu').val(ui.item.ibu); // save selected id to input
                    $('#jk').val(ui.item.jk); // save selected id to input

                    $('#penduduk').val(ui.item.id);
                    $('#kk').val(ui.item.kk_id);
                    $('#ayah').val(ui.item.id);
                    $('#ibu').val(ui.item.id);
                    $('#jk').val(ui.item.jk);
                    return false;
                }
            });

        });
    </script>

    {{-- Autofill From Request Code --}}
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#request_code").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofillfromrequest') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            cari: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#request_code').val(ui.item.label); // display the selected text
                    $('#tgl_pelaporan').val(ui.item.value); // save selected id to input
                    $('#tmpt_meninggal').val(ui.item.tmp_mati); // display the selected text
                    $('#tgl_meninggal').val(ui.item.tgl_mati); // save selected id to input
                    $('#jam_meninggal').val(ui.item.jam_mati); // save selected id to input
                    $('#penyebab_meninggal').val(ui.item.sebab_mati); // save selected id to input
                    $('#menerangkan').val(ui.item.menerangkan); // save selected id to input

                    $('#tgl_pel').val(ui.item.tgl_pel);
                    $('#tgl_mati').val(ui.item.tgl_mati);
                    $('#tmpt_mati').val(ui.item.tmpt_mati);
                    $('#jam_mati').val(ui.item.jam_mati);
                    $('#sebab_mati').val(ui.item.sebab_mati);
                    $('#menerangkan').val(ui.item.menerangkan);
                    return false;
                }
            });

        });
    </script>
@endpush
