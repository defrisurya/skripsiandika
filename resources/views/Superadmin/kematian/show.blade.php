@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kematian.index') }}">Data Kematian</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Kematian</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Detail Data Kematian Atas Nama : {{ $data->penduduk->nama }}</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form">
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Kelurahan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Dusun</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->dusun->nama_dusun }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Nomor KK</label>
                                                <input type="text" class="form-control" value="{{ $data->kk->no_kk }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">NIK</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->nik }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->nama }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Lahir</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->tempat_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tanggal Lahir</label>
                                                <input type="date" class="form-control"
                                                    value="{{ $data->penduduk->tanggal_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelamin</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->jenis_kelamin }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Agama</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->agama }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                @php
                                                    $tgl_lahir = new DateTime($data->penduduk->tanggal_lahir);
                                                    $today = new DateTime();
                                                    $umur = $today->diff($tgl_lahir);
                                                @endphp
                                                <label class="form-control-label" for="pekerjaan">Umur</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $umur->y }} Tahun" readonly>
                                            </div>
                                            <div class="col-3"><label class="form-control-label" for="pekerjaan">Tanggal
                                                    Meninggal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ date('d/m/Y', strtotime($data->tanggal_kematian)) }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Meninggal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->tempat_kematian }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jam Meninggal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->jam_kematian }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $ayah = App\Penduduk::where('id', $data->ayah)->first();
                                        $ibu = App\Penduduk::where('id', $data->ibu)->first();
                                        $pelapor = App\Penduduk::where('id', $data->pelapor)->first();
                                        $saksi1 = App\Penduduk::where('id', $data->saksi1)->first();
                                        $saksi2 = App\Penduduk::where('id', $data->saksi2)->first();
                                    @endphp
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Tanggal
                                                    Pelaporan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ date('d/m/Y', strtotime($data->tanggal_pelaporan)) }}"
                                                    readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Pelapor</label>
                                                <input type="text" class="form-control" value="{{ $pelapor->nama }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Saksi 1</label>
                                                <input type="text" class="form-control" value="{{ $saksi1->nama }}"
                                                    readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Saksi 2</label>
                                                <input type="text" class="form-control" value="{{ $saksi2->nama }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Ayah</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $ayah->nama_ayah }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Ibu</label>
                                                <input type="text" class="form-control" value="{{ $ibu->nama_ibu }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Penyebab
                                                    Meninggal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penyebab_kematian }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Yang Menerangkan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->menerangkan }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
