@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Data Kematian</h6>
        </nav>

        <a class="btn bg-gradient-info w-25 mt-4 mb-3" href="{{ route('kematian.create') }}"><i
                class="fas fa-plus"></i>&nbsp;
            Tambah Data Kematian</a>

        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0"></div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Tanggal Pelaporan</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            NIK</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Nama</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Tempat, Tanggal Lahir</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Umur</th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $item)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">
                                                            {{ date('d/m/Y', strtotime($item->tanggal_pelaporan)) }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $item->penduduk->nik }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $item->penduduk->nama }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">{{ $item->penduduk->tempat_lahir }},
                                                            {{ date('d/m/Y', strtotime($item->penduduk->tanggal_lahir)) }}
                                                        </h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex px-2 py-1">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm">
                                                            @php
                                                                $tgl_lahir = new DateTime($item->penduduk->tanggal_lahir);
                                                                $today = new DateTime();
                                                                $umur = $today->diff($tgl_lahir);
                                                            @endphp
                                                            {{ $umur->y }} tahun
                                                        </h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                        data-bs-toggle="dropdown" aria-expanded="false">
                                                        Action
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{ route('kematian.print', $item->id) }}"
                                                                class="dropdown-item" target="__blank"><i
                                                                    class="fas fa-print"></i>
                                                                Print</a>
                                                        </li>
                                                        <li><a href="{{ route('kematian.edit', $item->id) }}"
                                                                class="dropdown-item"><i class="fas fa-edit"></i>
                                                                Edit</a>
                                                        </li>
                                                        <li><a href="{{ route('kematian.show', $item->id) }}"
                                                                class="dropdown-item"><i class="fas fa-eye"></i>
                                                                Detail</a>
                                                        </li>
                                                        {{-- <form action="{{ route('kematian.destroy', $item->id) }}"
                                                            method="POST"
                                                            onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                            {!! method_field('delete') . csrf_field() !!}
                                                            <li><button class="dropdown-item" type="submit">
                                                                    <i class="fas fa-trash"></i> Delete
                                                                </button></li>
                                                        </form> --}}
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <h6 class="text-secondary"><i>Data Masih Kosong!</i></h6>
                                            </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
