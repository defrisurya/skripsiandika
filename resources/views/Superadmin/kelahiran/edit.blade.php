@extends('layouts.front.backend')

@section('css')
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kelahiran.index') }}" style="text-decoration: none">Data Kelahiran</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Data Kelahiran</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Data Kelahiran</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('kelahiran.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                {{-- <select class="form-control" name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select> --}}
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control" name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option {{ $data->dusun_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Anak</label>
                                                <input type="text"
                                                    class="form-control @error('nama_anak') is-invalid @enderror"
                                                    name="nama_anak" id="nama_anak" placeholder="Nama Anak"
                                                    aria-label="nama_anak" aria-describedby="nama_anak-addon"
                                                    value="{{ old('nama_anak', $data->nama_anak) }}">
                                                @error('nama_anak')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Anak tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Anak Ke -</label>
                                                <input type="number"
                                                    class="form-control @error('anak_ke') is-invalid @enderror"
                                                    name="anak_ke" id="anak_ke" placeholder="Anak Ke" aria-label="anak_ke"
                                                    aria-describedby="anak_ke-addon"
                                                    value="{{ old('anak_ke', $data->anak_ke) }}">
                                                @error('anak_ke')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Anak Ke tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Tempat Lahir</label>
                                                <input type="text"
                                                    class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                    name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir"
                                                    aria-label="tempat_lahir" aria-describedby="tempat_lahir-addon"
                                                    value="{{ old('tempat_lahir', $data->tempat_lahir) }}">
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label for="">Tanggal Lahir</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                    name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir"
                                                    aria-label="tanggal_lahir" aria-describedby="tanggal_lahir-addon"
                                                    value="{{ old('tanggal_lahir', $data->tanggal_lahir) }}">
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Jenis Kelamin</label>
                                                <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    name="jenis_kelamin" style="color: black">
                                                    <option selected disabled>-- Pilih Jenis Kelamin --</option>
                                                    <option {{ $data->jenis_kelamin ? 'selected' : '' }}
                                                        value="Laki-Laki" {{ old('jenis_kelamin') }}>Laki-laki
                                                    </option>
                                                    <option {{ $data->jenis_kelamin ? 'selected' : '' }}
                                                        value="Perempuan" {{ old('jenis_kelamin') }}>Perempuan
                                                    </option>
                                                </select>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Kelamin tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Agama</label>
                                                <select class="form-control @error('agama') is-invalid @enderror"
                                                    name="agama" style="color: black">
                                                    <option selected disabled>-- Pilih Agama --</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Islam"
                                                        {{ old('agama') }}>Islam</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Kristen"
                                                        {{ old('agama') }}>Kristen</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Katholik"
                                                        {{ old('agama') }}>Katholik</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Hindu"
                                                        {{ old('agama') }}>Hindu</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Buddha"
                                                        {{ old('agama') }}>Buddha</option>
                                                    <option {{ $data->agama ? 'selected' : '' }} value="Khonghucu"
                                                        {{ old('agama') }}>Khonghucu</option>
                                                </select>
                                                @error('agama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Agama tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Panjang <i>(cm)</i></label>
                                                <input type="number"
                                                    class="form-control @error('panjang') is-invalid @enderror"
                                                    name="panjang" id="panjang" placeholder="Panjang Anak"
                                                    aria-label="panjang" aria-describedby="panjang-addon"
                                                    value="{{ old('panjang', $data->panjang) }}">
                                                @error('panjang')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Panjang Anak tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label for="">Berat <i>(gram)</i></label>
                                                <input type="number"
                                                    class="form-control @error('berat') is-invalid @enderror"
                                                    name="berat" id="berat" placeholder="Berat Anak"
                                                    aria-label="berat" aria-describedby="berat-addon"
                                                    value="{{ old('berat', $data->berat) }}">
                                                @error('berat')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Berat Anak tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Kondisi Saat Lahir</label>
                                                <input type="text"
                                                    class="form-control @error('kondisi_lahir') is-invalid @enderror"
                                                    name="kondisi_lahir" id="kondisi_lahir" placeholder="Kondisi Lahir"
                                                    aria-label="kondisi_lahir" aria-describedby="kondisi_lahir-addon"
                                                    value="{{ old('kondisi_lahir', $data->kondisi_lahir) }}">
                                                @error('kondisi_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kondisi Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Jenis Kelahiran</label>
                                                <select class="form-control @error('jenis_kelahiran') is-invalid @enderror"
                                                    name="jenis_kelahiran" style="color: black">
                                                    <option selected disabled>-- Pilih Jenis Kelahiran --</option>
                                                    <option {{ $data->jenis_kelahiran ? 'selected' : '' }}
                                                        value="Tunggal" {{ old('jenis_kelahiran') }}>Tunggal
                                                    </option>
                                                    <option {{ $data->jenis_kelahiran ? 'selected' : '' }}
                                                        value="Kembar" {{ old('jenis_kelahiran') }}>Kembar
                                                    </option>
                                                </select>
                                                @error('jenis_kelahiran')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Kelahiran tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Tempat Melahirkan</label>
                                                <select
                                                    class="form-control @error('tempat_melahirkan') is-invalid @enderror"
                                                    name="tempat_melahirkan" style="color: black">
                                                    <option selected disabled>-- Pilih Tempat Melahirkan --</option>
                                                    <option {{ $data->tempat_melahirkan ? 'selected' : '' }}
                                                        value="Rumah Sakit" {{ old('tempat_melahirkan') }}>Rumah
                                                        Sakit
                                                    </option>
                                                    <option {{ $data->tempat_melahirkan ? 'selected' : '' }}
                                                        value="Bidan" {{ old('tempat_melahirkan') }}>Bidan
                                                    </option>
                                                </select>
                                                @error('tempat_melahirkan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Melahirkan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Nama Ayah</label>
                                                <select class="form-control @error('nama_ayah') is-invalid @enderror"
                                                    name="nama_ayah" style="color: black" id="select-ayah">
                                                    <option selected disabled>-- Pilih Nama Ayah --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option {{ $data->nama_ayah == $item->nama ? 'selected' : '' }}
                                                            value="{{ $item->nama }}">{{ $item->nik }} -
                                                            {{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                                @error('nama_ayah')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Ayah tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Ibu</label>
                                                <select class="form-control @error('nama_ibu') is-invalid @enderror"
                                                    name="nama_ibu" style="color: black" id="select-ibu">
                                                    <option selected disabled>-- Pilih Nama Ibu --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option {{ $data->nama_ibu == $item->nama ? 'selected' : '' }}
                                                            value="{{ $item->nama }}">{{ $item->nik }} -
                                                            {{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                                @error('nama_ibu')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Ibu tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Kewarganegaraan</label>
                                                <select
                                                    class="form-control @error('kewarganegaraan') is-invalid @enderror"
                                                    name="kewarganegaraan" style="color: black">
                                                    <option selected disabled>-- Pilih Kewarganegaraan--</option>
                                                    <option {{ $data->kewarganegaraan ? 'selected' : '' }}
                                                        value="WNI" {{ old('kewarganegaraan') }}>WNI
                                                    </option>
                                                    <option {{ $data->kewarganegaraan ? 'selected' : '' }}
                                                        value="WNA" {{ old('kewarganegaraan') }}>WNA
                                                    </option>
                                                </select>
                                                @error('kewarganegaraan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kewarganegaraan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Alamat</label>
                                        <textarea name="alamat" id="alamat" class="form-control w-100 @error('alamat') is-invalid @enderror"
                                            rows="4" placeholder="Alamat">{{ old('alamat', $data->alamat) }}</textarea>
                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#kecamatan_id').select2({
            theme: 'bootstrap-5'
        });
        $('#kelurahan_id').select2({
            theme: 'bootstrap-5'
        });
        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });
        $('#select-ayah').select2({
            theme: 'bootstrap-5'
        });
        $('#select-ibu').select2({
            theme: 'bootstrap-5'
        });
    </script>
@endpush
