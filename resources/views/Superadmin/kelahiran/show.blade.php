@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('kelahiran.index') }}">Data Kelahiran</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Kelahiran</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Detail Data Kelahiran Atas Nama : {{ $data->nama_anak }}</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form">
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Kelurahan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Dusun</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->dusun->nama_dusun }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama Anak</label>
                                                <input type="text" class="form-control" value="{{ $data->nama_anak }}"
                                                    readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Anak Ke</label>
                                                <input type="text" class="form-control" value="{{ $data->anak_ke }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Lahir</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->tempat_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tanggal Lahir</label>
                                                <input type="date" class="form-control"
                                                    value="{{ $data->tanggal_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelamin</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->jenis_kelamin }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Agama</label>
                                                <input type="text" class="form-control" value="{{ $data->agama }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Panjang</label>
                                                <input type="text" class="form-control" value="{{ $data->panjang }} cm"
                                                    readonly>
                                            </div>
                                            <div class="col-3"><label class="form-control-label"
                                                    for="pekerjaan">Berat</label>
                                                <input type="text" class="form-control" value="{{ $data->berat }} gram"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kondisi Lahir</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kondisi_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelahiran</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->jenis_kelahiran }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Melahirkan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->tempat_melahirkan }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Nama Ayah</label>
                                                <input type="text" class="form-control" value="{{ $data->nama_ayah }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Nama Ibu</label>
                                                <input type="text" class="form-control" value="{{ $data->nama_ibu }}"
                                                    readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kewarganegaraan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kewarganegaraan }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Alamat</label>
                                                <input type="text" class="form-control" value="{{ $data->alamat }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
