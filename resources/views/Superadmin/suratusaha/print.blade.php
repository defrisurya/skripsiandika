<!DOCTYPE html>
<html>

<head>
    <title>Cetak Keterangan Usaha</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: center;
            font-size: 15px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }
    </style>
</head>

<body>
    <center>
        <table width="750">
            <tr>
                <td><img src="{{ asset('front') }}/home/img/kop_surat.png" width="100%" height="160"></td>
            </tr>
            <table width="750">
                <tr>
                    <td class="text2"><u><b>SURAT KETERANGAN USAHA</b></u></td>
                </tr>
            </table>
        </table>
        <table width="750">
            <tr class="text2">
                <td>Nomor</td>
                <td width="750">: -</td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Pemerintah Kelurahan Sindumartani, Kapanewon Ngempak. Kabupaten Sleman, dengan
                        ini
                        menyatakan bahwa :</font>
                </td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $data->penduduk->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $data->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Jenis Kelamin</td>
                <td width="525">: <b>{{ $data->jenis_kelamin }}</b></td>
            </tr>
            <tr>
                <td>4. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $data->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($data->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>5. Kewarganegaraan</td>
                <td width="525">: <b>{{ $data->warganegara }}</b></td>
            </tr>
            <tr>
                <td>6. Agama</td>
                <td width="525">: <b>{{ $data->agama }}</b></td>
            </tr>
            <tr>
                <td>7. Pekerjaan</td>
                <td width="525">: <b>{{ $data->pekerjaan }}</b></td>
            </tr>
            <tr>
                <td>8. Alamat</td>
                <td width="525">: <b>{{ $data->alamat }}</b></td>
            </tr>
            <tr>
                <td>9. Jenis Usaha</td>
                <td width="525">: <b>{{ $data->jenis_usaha }}</b></td>
            </tr>
            <tr>
                <td>10. Nama Usaha</td>
                <td width="525">: <b>{{ $data->nama_usaha }}</b></td>
            </tr>
            <tr>
                <td>11. Alamat Usaha</td>
                <td width="525">: <b>{{ $data->alamat_usaha }}</b></td>
            </tr>
        </table>
        <br>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Demikian surat Keterangan ini dibuat, agar dapat dipergunakan sebagaimana
                        mestinya.
                    </font>
                </td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td width="430"></td>
                <td class="text" align="center">Yogyakarta,
                    {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
            </tr>
            <tr>
                <td width="430"><br><br><br><br></td>
                <br>
                <br>
                <br>
                <br>
                <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS. ANANG
                            ZAMRONI, M.S.I.</b></u></td>
            </tr>
        </table>
    </center>

    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>
