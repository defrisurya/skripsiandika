@extends('layouts.front.backend')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
        integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('suratusaha.index') }}" style="text-decoration: none">Data Surat Keterangan
                        Usaha</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Buat Surat Keterangan Usaha</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Buat Surat Keterangan Usaha</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <div class="mb-3">
                                    <div class="row g-3">
                                        <div class="col-4">
                                            <label for="">Kode Permintaan Surat</label>
                                            <input type="text" class="form-control" id="request_code">
                                        </div>
                                    </div>
                                </div>
                                <form role="form" method="POST" action="{{ route('suratusaha.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Nama Lengkap</label>
                                                <input type="hidden" name="penduduk_id" id="penduduk">
                                                <input type="text"
                                                    class="form-control @error('penduduk_id') is-invalid @enderror"id="penduduk_id"
                                                    placeholder="Nama Lengkap" value="{{ old('penduduk_id') }}">
                                                @error('penduduk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Lengkap tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">NIK</label>
                                                <input type="hidden" name="nik" id="niks">
                                                <input type="text"
                                                    class="form-control @error('nik') is-invalid @enderror" id="nik"
                                                    placeholder="Nomor Induk Keluarga" value="{{ old('nik') }}" readonly>
                                                @error('nik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nomor Induk Keluarga tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Jenis Kelamin</label>
                                                <input type="hidden" name="jenis_kelamin" id="jk">
                                                <input type="text"
                                                    class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    id="jenis_kelamin" placeholder="Jenis Kelamin"
                                                    value="{{ old('jenis_kelamin') }}" readonly>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Kelamin tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Tempat Lahir</label>
                                                <input type="hidden" name="tempat_lahir" id="tl">
                                                <input type="text"
                                                    class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                    id="tempat_lahir" placeholder="Tempat Lahir"
                                                    value="{{ old('tempat_lahir') }}" readonly>
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Tanggal Lahir</label>
                                                <input type="hidden" name="tanggal_lahir" id="tgl">
                                                <input type="text"
                                                    class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                    id="tanggal_lahir" placeholder="Tanggal Lahir"
                                                    value="{{ old('tanggal_lahir') }}" readonly>
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Kewarganegaraan</label>
                                                <input type="hidden" name="warganegara" id="negara">
                                                <input type="text"
                                                    class="form-control @error('warganegara') is-invalid @enderror"
                                                    id="warganegara" placeholder="Kewarganegaraan"
                                                    value="{{ old('warganegara') }}" readonly>
                                                @error('warganegara')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kewarganegaraan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Agama</label>
                                                <input type="hidden" name="agama" id="religion">
                                                <input type="text"
                                                    class="form-control @error('agama') is-invalid @enderror"
                                                    id="agama" placeholder="Agama" value="{{ old('agama') }}"
                                                    readonly>
                                                @error('agama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Agama tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Pekerjaan</label>
                                                <input type="hidden" name="pekerjaan" id="kerja">
                                                <input type="text"
                                                    class="form-control @error('pekerjaan') is-invalid @enderror"
                                                    id="pekerjaan" placeholder="Pekerjaan"
                                                    value="{{ old('pekerjaan') }}" readonly>
                                                @error('pekerjaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pekerjaan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Jenis Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('jenis_usaha') is-invalid @enderror"
                                                    name="jenis_usaha" id="jns_usaha" placeholder="Jenis Usaha"
                                                    value="{{ old('jenis_usaha') }}">
                                                @error('jenis_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('nama_usaha') is-invalid @enderror"
                                                    name="nama_usaha" id="nama" placeholder="Nama Usaha"
                                                    value="{{ old('nama_usaha') }}">
                                                @error('nama_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Alamat Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('alamat_usaha') is-invalid @enderror"
                                                    name="alamat_usaha" id="addres" placeholder="Alamat Usaha"
                                                    value="{{ old('alamat_usaha') }}">
                                                @error('alamat_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Alamat Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Alamat</label>
                                        <input type="hidden" name="alamat" id="address">
                                        <textarea id="alamat" class="form-control w-100 @error('alamat') is-invalid @enderror" rows="4"
                                            placeholder="Alamat" readonly>{{ old('alamat') }}</textarea>
                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="simpan"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"
        integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>

    <script>
        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });
    </script>

    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#penduduk_id").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofillsuratusaha') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#penduduk_id').val(ui.item.label); // display the selected text
                    $('#nik').val(ui.item.value); // save selected id to input
                    $('#jenis_kelamin').val(ui.item.jk); // save selected id to input
                    $('#tempat_lahir').val(ui.item.tl); // save selected id to input
                    $('#tanggal_lahir').val(ui.item.tgl); // save selected id to input
                    $('#agama').val(ui.item.agama); // save selected id to input
                    $('#warganegara').val(ui.item.warganegara); // save selected id to input
                    $('#pekerjaan').val(ui.item.kerja); // save selected id to input
                    $('#alamat').val(ui.item.alamat); // save selected id to input

                    $('#penduduk').val(ui.item.id);
                    $('#niks').val(ui.item.value);
                    $('#jk').val(ui.item.jk);
                    $('#tl').val(ui.item.tl);
                    $('#tgl').val(ui.item.tgl);
                    $('#religion').val(ui.item.agama);
                    $('#negara').val(ui.item.warganegara);
                    $('#kerja').val(ui.item.kerja);
                    $('#address').val(ui.item.alamat);
                    return false;
                }
            });

        });
    </script>

    {{-- Autofill From Request Code --}}
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#request_code").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofillfromrequestusaha') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            cari: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#request_code').val(ui.item.label); // display the selected text
                    $('#jns_usaha').val(ui.item.value); // save selected id to input
                    $('#addres').val(ui.item.addres); // save selected id to input
                    $('#nama').val(ui.item.nama); // save selected id to input
                    return false;
                }
            });

        });
    </script>
@endpush
