@extends('layouts.front.backend')

@section('css')
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('suratusaha.index') }}" style="text-decoration: none">Data Surat Keterangah
                        Usaha</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Surat Keterangan Usaha</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Surat Keterangan Usaha</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('suratusaha.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                                {{-- <select class="form-control" name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select> --}}
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control" name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option {{ $data->dusun_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Nama Lengkap</label>
                                                <input type="text"
                                                    class="form-control @error('nama_lengkap') is-invalid @enderror"
                                                    name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap"
                                                    value="{{ old('nama_lengkap', $data->penduduk->nama) }}">
                                                @error('nama_lengkap')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Lengkap tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">NIK</label>
                                                <input type="number"
                                                    class="form-control @error('nik') is-invalid @enderror" name="nik"
                                                    id="nik" placeholder="Nomor Induk Keluarga"
                                                    value="{{ old('nik', $data->nik) }}">
                                                @error('nik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nomor Induk Keluarga tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Jenis Kelamin</label>
                                                <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    name="jenis_kelamin" id="jenis_kelamin">
                                                    <option value="">-- Jenis Kelamin --</option>
                                                    <option {{ $data->jenis_kelamin == 'Laki-Laki' ? 'selected' : '' }}
                                                        value="Laki-Laki">Laki-Laki</option>
                                                    <option {{ $data->jenis_kelamin == 'Perempuan' ? 'selected' : '' }}
                                                        value="Perempuan">Perempuan</option>
                                                </select>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Kelamin tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Tempat Lahir</label>
                                                <input type="text"
                                                    class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                    name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir"
                                                    value="{{ old('tempat_lahir', $data->tempat_lahir) }}">
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tempat Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Tanggal Lahir</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                    name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir"
                                                    value="{{ old('tanggal_lahir', $data->tanggal_lahir) }}">
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Lahir tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Kewarganegaraan</label>
                                                <select class="form-control @error('warganegara') is-invalid @enderror"
                                                    name="warganegara" id="warganegara">
                                                    <option value="">-- Kewarganegaraan --</option>
                                                    <option {{ $data->warganegara == 'WNI' ? 'selected' : '' }}
                                                        value="WNI">WNI</option>
                                                    <option {{ $data->warganegara == 'WNA' ? 'selected' : '' }}
                                                        value="WNA">WNA</option>
                                                </select>
                                                @error('warganegara')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kewarganegaraan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Agama</label>
                                                <input type="text"
                                                    class="form-control @error('agama') is-invalid @enderror"
                                                    name="agama" id="agama" placeholder="Agama"
                                                    value="{{ old('agama', $data->agama) }}">
                                                @error('agama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Agama tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label for="">Pekerjaan</label>
                                                <input type="text"
                                                    class="form-control @error('pekerjaan') is-invalid @enderror"
                                                    name="pekerjaan" id="pekerjaan" placeholder="Pekerjaan"
                                                    value="{{ old('pekerjaan', $data->pekerjaan) }}">
                                                @error('pekerjaan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pekerjaan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Jenis Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('jenis_usaha') is-invalid @enderror"
                                                    name="jenis_usaha" id="jenis_usaha" placeholder="Jenis Usaha"
                                                    value="{{ old('jenis_usaha', $data->jenis_usaha) }}">
                                                @error('jenis_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jenis Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Nama Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('nama_usaha') is-invalid @enderror"
                                                    name="nama_usaha" id="nama_usaha" placeholder="Nama Usaha"
                                                    value="{{ old('nama_usaha', $data->nama_usaha) }}">
                                                @error('nama_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Nama Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Alamat Usaha</label>
                                                <input type="text"
                                                    class="form-control @error('alamat_usaha') is-invalid @enderror"
                                                    name="alamat_usaha" id="alamat_usaha" placeholder="Alamat Usaha"
                                                    value="{{ old('alamat_usaha', $data->alamat_usaha) }}">
                                                @error('alamat_usaha')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Alamat Usaha tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Alamat</label>
                                        <textarea name="alamat" id="alamat" class="form-control w-100 @error('alamat') is-invalid @enderror"
                                            rows="4" placeholder="Alamat">{{ old('alamat', $data->alamat) }}</textarea>
                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#kecamatan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#kelurahan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });

        $('#penduduk_id').select2({
            theme: 'bootstrap-5'
        });
        $('#jenis_kelamin').select2({
            theme: 'bootstrap-5'
        });
    </script>
@endpush
