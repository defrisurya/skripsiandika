@extends('layouts.front.backend')

@section('css')
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('pindah.index') }}" style="text-decoration: none">Data Pindah</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Data Pindah</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Data Pindah</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('pindah.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                {{-- <select class="form-control" name="kelurahan_id" id="kelurahan_id">
                                                    <option selected disabled>-- Pilih Kelurahan --</option>
                                                    @foreach ($kelurahan as $item)
                                                        <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                        </option>
                                                    @endforeach
                                                </select> --}}
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="hidden" name="jenis_kelamin"
                                                    value="{{ $data->jenis_kelamin }}">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control" name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option {{ $data->dusun_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label for="">Penduduk</label>
                                                <select class="form-control @error('penduduk_id') is-invalid @enderror"
                                                    name="penduduk_id" id="penduduk_id">
                                                    <option selected disabled>-- Pilih Penduduk --</option>
                                                    @foreach ($penduduk as $item)
                                                        <option {{ $data->penduduk_id == $item->id ? 'selected' : '' }}
                                                            value="{{ $item->id }}">{{ $item->nik }} -
                                                            {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('penduduk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Penduduk!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-4">
                                                <label for="">Tanggal Pindah</label>
                                                <input type="date"
                                                    class="form-control @error('tgl_pindah') is-invalid @enderror" name="tgl_pindah"
                                                    id="tgl_pindah" placeholder="Desa (Kelurahan)"
                                                    value="{{ old('tgl_pindah', $data->tgl_pindah) }}">
                                                @error('tgl_pindah')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Desa (Kelurahan) tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            <div class="col-4">
                                                <label for="">Desa (Kelurahan) Tujuan</label>
                                                <input type="text"
                                                    class="form-control @error('desa') is-invalid @enderror" name="desa"
                                                    id="desa" placeholder="Desa (Kelurahan)"
                                                    value="{{ old('desa', $data->desa) }}">
                                                @error('desa')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Desa (Kelurahan) tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label for="">Kecamatan Tujuan</label>
                                                <input type="text"
                                                    class="form-control @error('kecamatan') is-invalid @enderror"
                                                    name="kecamatan" id="kecamatan" placeholder="Kecamatan Tujuan"
                                                    value="{{ old('kecamatan', $data->kecamatan) }}">
                                                @error('kecamatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kecamatan Tujuan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Kabupaten Tujuan</label>
                                                <input type="text"
                                                    class="form-control @error('kabupaten') is-invalid @enderror"
                                                    name="kabupaten" id="kabupaten" placeholder="Kabupaten Tujuan"
                                                    value="{{ old('kabupaten', $data->kabupaten) }}">
                                                @error('kabupaten')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kabupaten Tujuan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Provinsi Tujuan</label>
                                                <input type="text"
                                                    class="form-control @error('provinsi') is-invalid @enderror"
                                                    name="provinsi" id="provinsi"
                                                    value="{{ old('provinsi', $data->provinsi) }}"
                                                    placeholder="Provinsi Tujuan">
                                                @error('provinsi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Provinsi Tujuan tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Jumlah Keluarga Yang Ikut</label>
                                                <input type="number"
                                                    class="form-control @error('jml_ikut') is-invalid @enderror"
                                                    name="jml_ikut" id="jml_ikut"
                                                    value="{{ old('jml_ikut', $data->jml_ikut) }}"
                                                    placeholder="Jumlah Keluarga Yang Ikut">
                                                @error('jml_ikut')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jumlah Keluarga Yang Ikut tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Alamat Tujuan</label>
                                        <textarea name="alamat_tujuan" id="alamat_tujuan"
                                            class="form-control w-100 @error('alamat_tujuan') is-invalid @enderror" rows="4"
                                            placeholder="Alamat Tujuan">{{ old('alamat_tujuan', $data->alamat_tujuan) }}</textarea>
                                        @error('alamat_tujuan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat Tujuan tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Alasan</label>
                                        <textarea name="alasan" id="alasan" class="form-control w-100 @error('alasan') is-invalid @enderror"
                                            rows="4" placeholder="Alasan">{{ old('alasan', $data->alasan) }}</textarea>
                                        @error('alasan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alasan tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#kecamatan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#kelurahan_id').select2({
            theme: 'bootstrap-5'
        });

        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });

        $('#penduduk_id').select2({
            theme: 'bootstrap-5'
        });
    </script>
@endpush
