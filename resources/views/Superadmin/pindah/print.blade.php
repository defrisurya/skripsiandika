<!DOCTYPE html>
<html>

<head>
    <title>Cetak Surat Pindah</title>
    <style type="text/css">
        table {
            border-style: double;
            border-width: 3px;
            border-color: white;
        }

        table tr .text2 {
            text-align: right;
            font-size: 15px;
        }

        table tr .text {
            text-align: center;
            font-size: 15px;
        }

        table tr td {
            font-size: 15px;
        }
    </style>
</head>

<body>
    <center>
        <table width="750">
            <tr>
                <td><img src="{{ asset('front') }}/home/img/kop_surat.png" width="100%" height="160"></td>
            </tr>
            <table width="750">
                <tr>
                    <td>Hal : Permohonan Pindah Penduduk</td>
                </tr>
            </table>
        </table>
        <table width="650" align="right">
            <tr align="text2">
                <td>Kepada<br>Yth. Bupati Sleman<br>Di Tempat</td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Dengan hormat</font>
                </td>
            </tr>
            <tr>
                <td>
                    <font size="3">Yang bertanda tangan dibawah ini :</font>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr class="text2">
                <td>1. Nama Lengkap</td>
                <td width="525">: <b>{{ $data->penduduk->nama }}</b></td>
            </tr>
            <tr>
                <td>2. Nomor Induk Kependudukan</td>
                <td width="525">: <b>{{ $data->penduduk->nik }}</b></td>
            </tr>
            <tr>
                <td>3. Jenis Kelamin</td>
                <td width="525">: <b>{{ $data->penduduk->jenis_kelamin }}</b></td>
            </tr>
            <tr>
                <td>4. Tempat/ Tanggal Lahir</td>
                <td width="525">: <b>{{ $data->penduduk->tempat_lahir }},
                        {{ date('d/m/Y', strtotime($data->penduduk->tanggal_lahir)) }}</b></td>
            </tr>
            <tr>
                <td>5. Kewarganegaraan</td>
                <td width="525">: <b>{{ $data->penduduk->kewarganegaraan }}</b></td>
            </tr>
            <tr>
                <td>6. Agama</td>
                <td width="525">: <b>{{ $data->penduduk->agama }}</b></td>
            </tr>
            <tr>
                <td>7. Pendidikan</td>
                <td width="525">: <b>{{ $data->penduduk->pendidikan }}</b></td>
            </tr>
            <tr>
                <td>8. Hubungan Keluarga</td>
                <td width="525">: <b>{{ $data->penduduk->hubungan_keluarga }}</b></td>
            </tr>
            <tr>
                <td>9. Pekerjaan</td>
                <td width="525">: <b>{{ $data->penduduk->pekerjaan }}</b></td>
            </tr>
            @php
                $penduduk = App\Penduduk::where('id', $data->penduduk->id)->first();
                $alamat = App\KK::where('id', $penduduk->kk_id)->first();
                // dd($alamat->alamat);
            @endphp
            <tr>
                <td>10. Alamat Lama</td>
                <td width="525">: <b>{{ $alamat->alamat }}</b></td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Mengajukan Permohonan Pindah Penduduk di alamat :</font>
                </td>
            </tr>
        </table>
        <table width="750">
            <tr class="text2">
                <td>11. Alamat Baru</td>
                <td width="525">: <b>{{ $data->alamat_tujuan }}</b></td>
            </tr>
            <tr>
                <td>12. Desa (Kelurahan)</td>
                <td width="525">: <b>{{ $data->desa }}</b></td>
            </tr>
            <tr>
                <td>13. Kecamatan</td>
                <td width="525">: <b>{{ $data->kecamatan }}</b></td>
            </tr>
            <tr>
                <td>14. Kabupaten/Kotamadya</td>
                <td width="525">: <b>{{ $data->kabupaten }}</b></td>
            </tr>
            <tr>
                <td>15. Provinsi/Negara</td>
                <td width="525">: <b>{{ $data->provinsi }}</b></td>
            </tr>
            <tr>
                <td>16. Alasan Pindah</td>
                <td width="525">: <b>{{ $data->alasan }}</b></td>
            </tr>
            <tr>
                <td>17. Jumlah Keluarga Yang Ikut</td>
                <td width="525">: <b>{{ $data->jml_ikut }} Orang</b></td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td>
                    <font size="3">Demikian surat Keterangan ini dibuat, agar dapat dipergunakan sebagaimana
                        mestinya.
                    </font>
                </td>
            </tr>
        </table>
        <br>
        <table width="750">
            <tr>
                <td width="430"></td>
                <td class="text" align="center">Yogyakarta,
                    {{ date('d/m/Y', strtotime(Carbon\Carbon::now())) }}</td>
            </tr>
            <tr>
                <td width="430"><br><br><br><br></td>
                <br>
                <br>
                <br>
                <br>
                <td class="text" align="center">Mengetahui<br>Lurah<br><br><br><br><br><br><br><br><u><b>DRS. ANANG
                            ZAMRONI, M.S.I.</b></u></td>
            </tr>
        </table>
    </center>

    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>
