@extends('layouts.front.backend')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
        integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <!-- Or for RTL support -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
@endsection

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('pendatang.index') }}" style="text-decoration: none">Data Pendatang</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Tambah Data Pendatang</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Tambah Data Pendatang</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('pendatang.store') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Nama Kelurahan</label>
                                                <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                                <input type="hidden" name="jenis_kelamin" id="jk">
                                                <input type="text" class="form-control"
                                                    value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label for="">Nama Dusun</label>
                                                <select class="form-control @error('dusun_id') is-invalid @enderror"
                                                    name="dusun_id" id="dusun_id">
                                                    <option selected disabled>-- Pilih Dusun --</option>
                                                    @foreach ($dusun as $item)
                                                        <option value="{{ $item->id }}">{{ $item->nama_dusun }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('dusun_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Dusun!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label for="">Penduduk</label>
                                                <input type="hidden" name="penduduk_id" id="penduduk">
                                                <input type="text"
                                                    class="form-control @error('penduduk_id') is-invalid @enderror"
                                                    id="penduduk_id">
                                                @error('penduduk_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Pilih Penduduk!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Tanggal Datang</label>
                                                <input type="date"
                                                    class="form-control @error('tanggal_datang') is-invalid @enderror"
                                                    name="tanggal_datang" id="tanggal_datang" placeholder="Tanggal Datang"
                                                    aria-label="tanggal_datang" aria-describedby="tanggal_datang-addon"
                                                    value="{{ old('tanggal_datang') }}">
                                                @error('tanggal_datang')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Tanggal Datang tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Asal Desa (Kelurahan)</label>
                                                <input type="text"
                                                    class="form-control @error('desa') is-invalid @enderror" name="desa"
                                                    id="desa" placeholder="Asal Desa (Kelurahan)"
                                                    value="{{ old('desa') }}">
                                                @error('desa')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Asal Desa (Kelurahan) tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label for="">Kecamatan Asal</label>
                                                <input type="text"
                                                    class="form-control @error('kecamatan') is-invalid @enderror"
                                                    name="kecamatan" id="kecamatan" placeholder="Kecamatan Asal"
                                                    value="{{ old('kecamatan') }}">
                                                @error('kecamatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kecamatan Asal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Kabupaten Asal</label>
                                                <input type="text"
                                                    class="form-control @error('kabupaten') is-invalid @enderror"
                                                    name="kabupaten" id="kabupaten" placeholder="Kabupaten Asal"
                                                    value="{{ old('kabupaten') }}">
                                                @error('kabupaten')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Kabupaten Asal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Provinsi Asal</label>
                                                <input type="text"
                                                    class="form-control @error('provinsi') is-invalid @enderror"
                                                    name="provinsi" id="provinsi" value="{{ old('provinsi') }}"
                                                    placeholder="Provinsi Asal">
                                                @error('provinsi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Provinsi Asal tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-3">
                                                <label for="">Jumlah Keluarga Yang Ikut</label>
                                                <input type="number"
                                                    class="form-control @error('jml_ikut') is-invalid @enderror"
                                                    name="jml_ikut" id="jml_ikut" value="{{ old('jml_ikut') }}"
                                                    placeholder="Jumlah Keluarga Yang Ikut">
                                                @error('jml_ikut')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Jumlah Keluarga Yang Ikut tidak boleh kosong!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <textarea name="alamat_asal" id="alamat_asal" class="form-control w-100 @error('alamat_asal') is-invalid @enderror"
                                            rows="4" placeholder="Alamat Asal">{{ old('alamat_asal') }}</textarea>
                                        @error('alamat_asal')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat Asal tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <textarea name="alamat_sekarang" id="alamat_sekarang"
                                            class="form-control w-100 @error('alamat_sekarang') is-invalid @enderror" rows="4"
                                            placeholder="Alamat Sekarang">{{ old('alamat_sekarang') }}</textarea>
                                        @error('alamat_sekarang')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alamat Sekarang tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <textarea name="alasan" id="alasan" class="form-control w-100 @error('alasan') is-invalid @enderror"
                                            rows="4" placeholder="Alasan">{{ old('alasan') }}</textarea>
                                        @error('alasan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Alasan tidak boleh kosong!</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="simpan"
                                            class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"
        integrity="sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=" crossorigin="anonymous"></script>

    <script>
        $('#dusun_id').select2({
            theme: 'bootstrap-5'
        });
    </script>
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {

            $("#penduduk_id").autocomplete({
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "{{ route('autofillsuratpendatang') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            _token: CSRF_TOKEN,
                            search: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    $('#penduduk_id').val(ui.item.label); // display the selected text
                    $('#kk_id').val(ui.item.value); // save selected id to input
                    $('#nama_ayah').val(ui.item.ayah); // save selected id to input
                    $('#nama_ibu').val(ui.item.ibu); // save selected id to input
                    $('#jk').val(ui.item.jk); // save selected id to input

                    $('#penduduk').val(ui.item.id);
                    $('#kk').val(ui.item.kk_id);
                    $('#ayah').val(ui.item.id);
                    $('#ibu').val(ui.item.id);
                    $('#jk').val(ui.item.jk);
                    return false;
                }
            });

        });
    </script>
@endpush
