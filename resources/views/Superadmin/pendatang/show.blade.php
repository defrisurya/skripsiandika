@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('pendatang.index') }}">Data Pendatang</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Detail Data Pendatang</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Detail Data Pendatang Atas Nama : {{ $data->penduduk->nama }}</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form">
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Kelurahan</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kelurahan->nama_kelurahan }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Dusun</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->dusun->nama_dusun }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">NIK</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->nik }}" readonly>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-control-label" for="pekerjaan">Nama</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->nama }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tempat Lahir</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->tempat_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Tanggal Lahir</label>
                                                <input type="date" class="form-control"
                                                    value="{{ $data->penduduk->tanggal_lahir }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jenis Kelamin</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->jenis_kelamin }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Agama</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->penduduk->agama }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                @php
                                                    $tgl_lahir = new DateTime($data->penduduk->tanggal_lahir);
                                                    $today = new DateTime();
                                                    $umur = $today->diff($tgl_lahir);
                                                @endphp
                                                <label class="form-control-label" for="pekerjaan">Umur</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $umur->y }} Tahun" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Tanggal
                                                    Datang</label>
                                                <input type="text" class="form-control"
                                                    value="{{ date('d/m/Y', strtotime($data->tanggal_datang)) }}"
                                                    readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Desa (Kelurahan)
                                                    Asal</label>
                                                <input type="text" class="form-control" value="{{ $data->desa }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kabupaten Asal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->kabupaten }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Kecamatan Asal</label>
                                                <input type="date" class="form-control"
                                                    value="{{ $data->kecamatan }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Provinsi Asal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->provinsi }}" readonly>
                                            </div>
                                            <div class="col-3">
                                                <label class="form-control-label" for="pekerjaan">Jumlah Keluarga Yang
                                                    Ikut</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->jml_ikut }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row g-3">
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Alamat Asal</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->alamat_asal }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Alamat Sekarang</label>
                                                <input type="text" class="form-control"
                                                    value="{{ $data->alamat_sekarang }}" readonly>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-control-label" for="pekerjaan">Alasan</label>
                                                <input type="text" class="form-control" value="{{ $data->alasan }}"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
