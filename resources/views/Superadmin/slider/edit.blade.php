@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('slider.index') }}" style="text-decoration: none">Slider</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Slider</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Slider</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('slider.update', $slider->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="foto">Foto</label><br>
                                        <input class="form-control" type="file" name="foto1" id="foto">
                                        <div class="form-group mt-2" style="max-width: 20rem;">
                                            <img width="300" src="{{ asset($slider->foto1) }}" alt="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Foto</label><br>
                                        <input class="form-control" type="file" name="foto2" id="foto">
                                        <div class="form-group mt-2" style="max-width: 20rem;">
                                            <img width="300" src="{{ asset($slider->foto2) }}" alt="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Foto</label><br>
                                        <input class="form-control" type="file" name="foto3" id="foto">
                                        <div class="form-group mt-2" style="max-width: 20rem;">
                                            <img width="300" src="{{ asset($slider->foto3) }}" alt="">
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
