@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}"
                        style="text-decoration: none">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('profile.index') }}" style="text-decoration: none">Profile</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Profile</h6>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 mb-3">
                        <h6>Edit Profile</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('profile.update', $profil->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="foto">Alamat</label><br>
                                        <input class="form-control" type="text" name="alamat"
                                            value="{{ $profil->alamat }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Telephon</label><br>
                                        <input class="form-control" type="text" name="tlp"
                                            value="{{ $profil->tlp }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Email</label><br>
                                        <input class="form-control" type="text" name="email"
                                            value="{{ $profil->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Jam Kerja Awal</label><br>
                                        <input class="form-control" type="time" name="jam_kerja_awal"
                                            value="{{ $profil->jam_kerja_awal }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="foto">Jam Kerja Akhir</label><br>
                                        <input class="form-control" type="time" name="jam_kerja_akhir"
                                            value="{{ $profil->jam_kerja_akhir }}">
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
