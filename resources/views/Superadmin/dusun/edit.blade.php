@extends('layouts.front.backend')

@section('content')
    <div class="container-fluid py-4">
        <nav aria-label="breadcrumb" style="margin-bottom: 10px">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item text-sm text-dark active" aria-current="page"><a class="opacity-5 text-dark"
                        href="{{ route('dusun.index') }}">Data Dusun</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">Edit Data Dusun</h6>
        </nav>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-6">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Edit Dusun</h6>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="container">
                                <form role="form" method="POST" action="{{ route('dusun.update', $data->id) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            {{-- <select class="form-control" name="kelurahan_id" id="kelurahan_id">
                                                <option selected disabled>-- Pilih Kelurahan --</option>
                                                @foreach ($kelurahan as $item)
                                                    <option {{ $data->kelurahan_id == $item->id ? 'selected' : '' }}
                                                        value="{{ $item->id }}">{{ $item->nama_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select> --}}
                                            <label for="">Nama Kelurahan</label>
                                            <input type="hidden" name="kelurahan_id" value="{{ $kelurahan->id }}">
                                            <input type="text" class="form-control"
                                                value="{{ $kelurahan->nama_kelurahan }}" readonly>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <label for="">Nama Dusun</label>
                                            <input type="text" class="form-control" name="nama_dusun" id="nama_dusun"
                                                placeholder="Nama Dusun" aria-label="nama_dusun"
                                                aria-describedby="nama_dusun-addon"
                                                value="{{ old('nama_dusun', $data->nama_dusun) }}">
                                        </div>
                                        <div class="text-center">
                                            <button type="submit"
                                                class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
@endsection
