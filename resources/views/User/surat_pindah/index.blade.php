@extends('layouts.front.frontend')

@section('title', 'Aplikasi ..... | Surat Pindah')

@section('css')
    <style>
        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <!-- Start Bottom Header -->
    <div class="page-area"
        style="background-image: url({{ $slider->foto1 }}); background-repeat: no-repeat; background-size: cover; background-position: top center; backgroundpattachment: fixed;">
        <div class="home-overly"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="slider-content text-center">
                        <div class="header-bottom">
                            <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                                <h1 class="title2">Surat Online</h1>
                            </div>
                            <div class="layer3 wow zoomInUp" data-wow-duration="2s" data-wow-delay="1s">
                                <h2 class="title3">Surat Pindah</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Header -->
    <div class="blog-page area-padding">
        <div class="container">
            <div class="row">
                <!-- Start single blog -->
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="row">
                        <div class="col-md-11 col-sm-11 col-xs-11">
                            <div class="single-blog">
                                <div class="single-blog-img">
                                    <img src="{{ asset('front') }}/home/img/gallery/pindah.jpeg" alt="">
                                </div>
                                <div class="blog-text">
                                    <h4 style="margin-top: 20px">Pengajuan Surat Online "Surat Pindah"</h4>
                                    <p>Pengajuan surat online dapat dilakukan kapan saja dan dimana saja, yang dapat diakses
                                        melalui smartphone, Komputer, Laptop. Sebelum anda dapat melakukan pengajuan surat
                                        online "Surat Pindah", siapkan berkas-berkas yang diperlukan terlebih dahulu
                                        sesaui dengan persyaratan dibawah ini.</p>
                                    <h5>Persyaratan :</h5>
                                    <p>1. Upload Foto KTP</p>
                                    <p>2. Upload Foto KK</p>
                                    <p>3. Melengkapi form yang tersedia</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="single-blog">
                                <h3 style="margin-top: 10px">Permintaan Surat Online</h3>
                                <hr>
                                {{-- {{ dd($user->id) }} --}}
                                @if (auth()->user() != null)
                                    <div class="text-center">
                                        <button id="myBtn" class="btn btn-primary">Ajukan Permintaan Surat</button>
                                    </div>
                                    <div id="myModal" class="modal">
                                        <!-- Modal content -->
                                        <div class="modal-content">
                                            <span class="close">&times;</span>
                                            <p>Form Permintaan Surat</p>
                                            <form method="POST" action="{{ route('reqsuratonlinepindah.store') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div style="margin-top: 10px">
                                                    <input type="hidden" name="user_id" class="form-control"
                                                        value="{{ auth()->user()->id }}">
                                                    <input type="hidden" name="keterangan" class="form-control"
                                                        value="Surat Pindah">
                                                    <input type="hidden" name="status" class="form-control"
                                                        value="Proses">
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Nama Peminta Surat
                                                            </label>
                                                            <input type="text" name="nama"
                                                                class="form-control @error('nama') is-invalid @enderror"
                                                                placeholder="Nama Peminta Surat" required>
                                                            @error('nama')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Nama Peminta Surat tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="recipient-name" class="col-form-label">
                                                                Foto KTP Penduduk
                                                                <small>
                                                                    <i style="color: red">
                                                                        <b>
                                                                            Maks Size 1 Mb
                                                                        </b>
                                                                        <b>*</b>
                                                                    </i>
                                                                </small>
                                                            </label>
                                                            <input type="file" name="foto_ktp"
                                                                class="form-control @error('foto_ktp') is-invalid @enderror"
                                                                required>
                                                        </div>
                                                        @error('foto_ktp')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>Foto KTP tidak boleh kosong!</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Foto KK Penduduk
                                                                <small>
                                                                    <i style="color: red">
                                                                        <b>
                                                                            Maks Size 1 Mb
                                                                        </b>
                                                                        <b>*</b>
                                                                    </i>
                                                                </small>
                                                            </label>
                                                            <input type="file" name="foto_kk"
                                                                class="form-control @error('foto_kk') is-invalid @enderror"
                                                                required>
                                                        </div>
                                                        @error('foto_kk')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>Foto KK tidak boleh kosong!</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Desa Tujuan
                                                            </label>
                                                            <input type="text" name="desa"
                                                                class="form-control @error('desa') is-invalid @enderror"
                                                                placeholder="Desa Tujuan" required>
                                                            @error('desa')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Desa Tujuan tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Kecamatan Tujuan
                                                            </label>
                                                            <input type="text" name="kecamatan"
                                                                class="form-control @error('kecamatan') is-invalid @enderror"
                                                                placeholder="Kecamatan Tujuan" required>
                                                            @error('kecamatan')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Kecamatan Tujuan tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Kabupaten Tujuan
                                                            </label>
                                                            <input type="text" name="kabupaten"
                                                                class="form-control @error('kabupaten') is-invalid @enderror"
                                                                placeholder="Kabupaten Tujuan" required>
                                                            @error('kabupaten')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Kabupaten Tujuan tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Provinsi Tujuan
                                                            </label>
                                                            <input type="text" name="provinsi"
                                                                class="form-control @error('provinsi') is-invalid @enderror"
                                                                placeholder="Provinsi Tujuan" required>
                                                            @error('provinsi')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Provinsi Tujuan tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Alamat Tujuan
                                                            </label>
                                                            <input type="text" name="alamat_tujuan"
                                                                class="form-control @error('alamat_tujuan') is-invalid @enderror"
                                                                placeholder="Alamat Tujuan" required>
                                                            @error('alamat_tujuan')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Alamat Tujuan tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Alasan Pindah
                                                            </label>
                                                            <input type="text" name="alasan"
                                                                class="form-control @error('alasan') is-invalid @enderror"
                                                                placeholder="Alasan Pindah" required>
                                                            @error('alasan')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Alasan Pindah tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="input-group" style="margin-top: 10px">
                                                            <label for="message-text" class="col-form-label">
                                                                Jumlah Yang Ikut
                                                            </label>
                                                            <input type="number" name="jml_ikut"
                                                                class="form-control @error('jml_ikut') is-invalid @enderror"
                                                                placeholder="0" required>
                                                            @error('jml_ikut')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>Jumlah Yang Ikut tidak boleh kosong!</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center" style="margin-top: 10px">
                                                    <button type="submit" class="btn btn-primary">Kirim</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @else
                                    <div class="text-center" style="margin-top: 30px">
                                        <a href="{{ route('register') }}" class="ready-btn">Silahkan Lakukan
                                            Pendaftaran Akun</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Blog Area -->
@endsection

@push('script')
    <script>
        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
@endpush
