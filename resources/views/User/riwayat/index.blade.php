@extends('layouts.front.backendUser')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <div class="container">
        <div class="col-lg-12 grid-margin stretch-card mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Riwayat Permintaan Surat Online</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Foto KTP
                                    </th>
                                    <th>
                                        Foto KK
                                    </th>
                                    <th>
                                        Permintaan
                                    </th>
                                    <th>
                                        Selesai Pada
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $item)
                                    <tr>
                                        <td class="py-1">
                                            <img src="{{ asset($item->foto_ktp) }}" alt="image" />
                                        </td>
                                        <td class="py-1">
                                            <img src="{{ asset($item->foto_kk) }}" alt="image" />
                                        </td>
                                        <td>
                                            <label class="badge badge-primary">{{ $item->keterangan }}</label>
                                        </td>
                                        <td>
                                            @php
                                                $currentDate = date('H:i', strtotime($item->created_at));
                                                $startDate = date('H:i', strtotime($profil->jam_kerja_awal));
                                                $endDate = date('H:i', strtotime($profil->jam_kerja_akhir));
                                            @endphp
                                            <label class="badge badge-primary">
                                                @if (($currentDate >= $startDate) & ($currentDate <= $endDate))
                                                    Pukul {!! date('H:i', strtotime($item->created_at . ' +1 hours')) !!} WIB
                                                @else
                                                    {!! date('d-M-Y', strtotime($item->created_at . ' +1 days')) !!} | Pukul {{ date('H:i', strtotime('09:00:00')) }}
                                                    WIB
                                                @endif
                                            </label>
                                        </td>
                                        <td>
                                            @if ($item->status != 'Selesai')
                                                <span class="badge badge-danger">{{ $item->status }}</span>
                                            @else
                                                <span class="badge badge-success text-dark">{{ $item->status }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('reqsuratonline.show', $item->id) }}"
                                                style="text-decoration: none"><i class="fas fa-eye"></i>
                                                Detail</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4"><strong><i>Tidak ada Riwayat</i></strong></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
