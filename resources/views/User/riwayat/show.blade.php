@extends('layouts.front.backendUser')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <div class="container">
        <div class="col-md-12 grid-margin stretch-card mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Detail Permintaan Surat</h4>
                    <form class="forms-sample">
                        <div class="form-group row mt-5">
                            <div class="col">
                                <label for="exampleInputUsername1">Foto KTP</label> <br>
                                <img src="{{ asset($data->foto_ktp) }}" alt="" style="width: 150px; height:150px">
                            </div>
                            <div class="col">
                                <label for="exampleInputEmail1">Foto Kartu Keluarga</label> <br>
                                <img src="{{ asset($data->foto_kk) }}" alt="" style="width: 150px; height:150px">
                            </div>
                        </div>
                        <div class="form-group row mt-5">
                            <div class="col">
                                <label for="exampleInputConfirmPassword1">Permintaan Surat</label> <br>
                                <label class="badge badge-primary">{{ $data->keterangan }}</label>
                            </div>
                            <div class="col">
                                @php
                                    $currentDate = date('H:i', strtotime($data->created_at));
                                    $startDate = date('H:i', strtotime($profil->jam_kerja_awal));
                                    $endDate = date('H:i', strtotime($profil->jam_kerja_akhir));
                                    $result = $currentDate >= $startDate && $currentDate <= $endDate;
                                @endphp
                                <label for="exampleInputPassword1">Selesai Pada</label> <br>
                                <label class="badge badge-primary">
                                    @if ($result)
                                        {!! date('d-M-Y', strtotime($data->created_at . ' +1 days')) !!} | Pukul {{ date('H:i', strtotime('09:00:00')) }}
                                        WIB
                                    @else
                                        Pukul {!! date('H:i', strtotime($data->created_at . ' +1 hours')) !!} WIB
                                    @endif
                                </label>
                            </div>
                            <div class="col">
                                <label for="exampleInputConfirmPassword1">Status</label> <br>
                                <label class="badge badge-danger">{{ $data->status }}</label>
                            </div>
                        </div>
                        <div class="mt-5">
                            <a href="{{ route('reqsuratonline.index') }}" class="btn btn-primary"><i
                                    class="fas fa-arrow-left"></i>
                                Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
