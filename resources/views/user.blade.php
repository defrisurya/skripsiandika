@extends('layouts.front.backendUser')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="row">
                        <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                            <h4 class="font-weight-bold">Hallo! {{ auth()->user()->name }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="row">
                        <div class="col-12 col-xl-12 mb-12 mb-xl-0">
                            <h2 class="font-weight-bold text-center">Selamat Datang Di Halaman Dashboard Anda!</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        @include('layouts.front.componentUser.footer')
        <!-- partial -->
    </div>
@endsection
