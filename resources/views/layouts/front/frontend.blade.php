<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png" rel="icon">
    <link href="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900"
        rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('front') }}/home/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ asset('front') }}/home/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
    <link href="{{ asset('front') }}/home/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="{{ asset('front') }}/home/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
    <link href="{{ asset('front') }}/home/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('front') }}/home/lib/animate/animate.min.css" rel="stylesheet">
    <link href="{{ asset('front') }}/home/lib/venobox/venobox.css" rel="stylesheet">

    <!-- Nivo Slider Theme -->
    <link href="{{ asset('front') }}/home/css/nivo-slider-theme.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('front') }}/home/css/style.css" rel="stylesheet">

    <!-- Responsive Stylesheet File -->
    <link href="{{ asset('front') }}/home/css/responsive.css" rel="stylesheet">
    @yield('css')
</head>

<body data-spy="scroll" data-target="#navbar-example">

    <div id="preloader"></div>

    @include('layouts.front.components.navbar')

    @yield('content')

    @include('layouts.front.components.footer')

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="{{ asset('front') }}/home/lib/jquery/jquery.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/venobox/venobox.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/knob/jquery.knob.js"></script>
    <script src="{{ asset('front') }}/home/lib/wow/wow.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/parallax/parallax.js"></script>
    <script src="{{ asset('front') }}/home/lib/easing/easing.min.js"></script>
    <script src="{{ asset('front') }}/home/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="{{ asset('front') }}/home/lib/appear/jquery.appear.js"></script>
    <script src="{{ asset('front') }}/home/lib/isotope/isotope.pkgd.min.js"></script>

    <!-- Contact Form JavaScript File -->
    <script src="{{ asset('front') }}/home/contactform/contactform.js"></script>

    <script src="{{ asset('front') }}/home/js/main.js"></script>
    @stack('script')
</body>

</html>
