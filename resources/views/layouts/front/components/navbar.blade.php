<header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <!-- Navigation -->
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Brand -->
                            <a class="navbar-brand page-scroll sticky-logo" href="{{ route('welcome') }}">

                                <!-- Uncomment below if you prefer to use an image logo -->
                                <h1><img src="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png" alt=""
                                        title="" style="max-width: 40px"> <span>Kalurahan</span> Sindumartani</h1>
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1"
                            id="navbar-example">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="{{ Request::is('/*') ? 'active' : '' }}">
                                    <a class="page-scroll" href="{{ route('welcome') }}">Home</a>
                                </li>
                                {{-- <li class="dropdown"><a href="#" class="dropdown-toggle"
                                        data-toggle="dropdown">Profil<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Profil</a></li>
                                        <li><a href="#">Visi & Misi</a></li>
                                        <li><a href="#">Struktur Organisasi</a></li>
                                        <li><a href="#">Dusun</a></li>
                                    </ul>
                                </li> --}}
                                <li class="dropdown"><a href="#" class="dropdown-toggle"
                                        data-toggle="dropdown">Surat Online<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        {{-- <li><a href="{{ route('suratkelahiran') }}">Surat
                                                Kelahiran</a></li> --}}
                                        <li><a href="{{ route('suratkematian') }}">Surat Pengantar Akta Kematian</a>
                                        </li>
                                        <li><a href="{{ route('suratpindah') }}">Surat Pindah</a></li>
                                        <li><a href="{{ route('suratnikah') }}">Surat Pengantar Nikah</a></li>
                                        <li><a href="{{ route('suratizinusaha') }}">Surat Keterangan Usaha</a></li>
                                    </ul>
                                </li>
                                @if (auth()->user() != null)
                                    <span>
                                        <a href="{{ route('user') }}" class="page-scroll justify-content-center">
                                            <i class="fa fa-user-circle-o fa-2x"
                                                style="color: #fff; margin-top: 10px; padding-top: 9px; margin-left: 35px; margin-right: 15px;"
                                                aria-hidden="true"> {{ auth()->user()->name }}</i>
                                        </a>
                                    </span>
                                @else
                                    <span>
                                        <a class="page-scroll justify-content-center btn btn-primary"
                                            href="{{ route('register') }}"
                                            style="width: 100px; height:40px; margin-top: 15px; padding-top: 9px; border-radius: 7px; margin-left: 35px; margin-right: 15px">
                                            <b>Daftar</b>
                                        </a>
                                    </span>
                                    <span>
                                        <a class="page-scroll justify-content-center btn btn-primary"
                                            href="{{ route('login') }}"
                                            style="width: 100px; height:40px; margin-top: 15px; padding-top: 9px; border-radius: 7px">
                                            <b>Login</b>
                                        </a>
                                    </span>
                                @endif
                            </ul>
                        </div>
                        <!-- navbar-collapse -->
                    </nav>
                    <!-- END: Navigation -->
                </div>
            </div>
        </div>
    </div>
    <!-- header-area end -->
</header>
<!-- header end -->
