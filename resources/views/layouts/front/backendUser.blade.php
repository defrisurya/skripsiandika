<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aplikasi Kalurahan Sindumartani</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/vendors/feather/feather.css">
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('front') }}/assetsUser/js/select.dataTables.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('front') }}/assetsUser/css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png" />

    @yield('css')
</head>

<body>
    <div class="container-scroller">
        @include('layouts.front.componentUser.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            @include('layouts.front.componentUser.sidebar')
            <!-- partial -->
            @yield('content')
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="{{ asset('front') }}/assetsUser/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('front') }}/assetsUser/vendors/chart.js/Chart.min.js"></script>
    <script src="{{ asset('front') }}/assetsUser/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="{{ asset('front') }}/assetsUser/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/dataTables.select.min.js"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('front') }}/assetsUser/js/off-canvas.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/hoverable-collapse.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/template.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/settings.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('front') }}/assetsUser/js/dashboard.js"></script>
    <script src="{{ asset('front') }}/assetsUser/js/Chart.roundedBarCharts.js"></script>
    <!-- End custom js for this page-->

    @stack('script')
</body>

</html>
