<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png">
    <link rel="icon" type="image/png" href="{{ asset('front') }}/home/img/Lambang_Kabupaten_Sleman.png">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>
        Aplikasi Kalurahan Sindumartani
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('front') }}/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="{{ asset('front') }}/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('front') }}/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('front') }}/assets/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />

    @yield('css')
</head>

<body class="g-sidenav-show  bg-gray-100">

    @include('layouts.front.componentBackend.sidebar')

    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">

        @include('layouts.front.componentBackend.navbar')

        @include('sweetalert::alert')

        @yield('content')
    </main>

    <!--   Core JS Files   -->
    <script src="{{ asset('front') }}/assets/js/core/popper.min.js"></script>
    <script src="{{ asset('front') }}/assets/js/core/bootstrap.min.js"></script>
    <script src="{{ asset('front') }}/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('front') }}/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="{{ asset('front') }}/assets/js/plugins/chartjs.min.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('front') }}/assets/js/soft-ui-dashboard.min.js?v=1.0.3"></script>

    @stack('script')
</body>

</html>
