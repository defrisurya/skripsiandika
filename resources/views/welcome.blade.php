﻿@extends('layouts.front.frontend')

@section('title', 'Aplikasi Kalurahan Sindumartani')

@section('content')
    <!-- Start Slider Area -->
    <div id="home" class="slider-area">
        <div class="bend niceties preview-2">
            <div id="ensign-nivoslider" class="slides" style="height: 670px">
                <img src="{{ asset($slider->foto1) }}" alt="" title="#slider-direction-1" />
                <img src="{{ asset($slider->foto2) }}" alt="" title="#slider-direction-2" />
                <img src="{{ asset($slider->foto3) }}" alt="" title="#slider-direction-3" />
            </div>
        </div>
    </div>
    <!-- End Slider Area -->

    <!-- Start About area -->
    <div id="about" class="about-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Kalurahan Sindumartani</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- single-well start-->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="well-left">
                        <div class="single-well">
                            <img src="{{ asset('front') }}/home/img/sindumartani.jpeg" alt="">
                        </div>
                    </div>
                </div>
                <!-- single-well end-->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="well-middle">
                        <div class="single-well">
                            <a href="#">
                                <h4 class="sec-head">Profile</h4>
                            </a>
                            <p>
                                Sejarah mengenai Desa Sindumartani didapatkan dari hasil wawancara dengan seorang tokoh
                                masyarakat yang bernama R. Sugiyanto. Beliau dianggap sebagai sesepuh yang mengetahui
                                sejarah Desa Sindumartani. Selain itu sampai saat ini beliau juga menjabat sebagai ketua
                                LPMD Sindumartani. Menurut penuturan beliau, Desa Sindumartani terbentuk pada tahun 1946
                                yang merupakan gabungan dari tiga kelurahan yaitu kelurahan Johosari, Jambusari, dan
                                Pencarsari.
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End col-->
            </div>
        </div>
    </div>
    <!-- End About area -->

    <!-- Faq area start -->
    <div class="faq-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Faq Question</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="tab-menu">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active">
                                <a href="#p-view-1" role="tab" data-toggle="tab">VISI & MISI</a>
                            </li>
                            <li>
                                <a href="#p-view-2" role="tab" data-toggle="tab">Dusun</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="p-view-1">
                            <div class="tab-inner">
                                <div class="event-content head-team">
                                    <h4>VISI</h4>
                                    <p>
                                        “Terwujudnya Sindumartani Makmur yang Aman, Sehat, Cerdas, Produktif, Berbudaya dan
                                        Berakhlak Mulia”
                                    </p>
                                    <h4>MISI</h4>
                                    <p>

                                        <b>MAKMUR</b> <br>
                                        1. Menggali potensi sumber daya alam dan sumber daya manusia
                                        dalam mewujudkan
                                        kemakmuran warga dengan target yang terukur, melalui “Program Ekonomi Sindumartani”
                                        , <br>

                                        2. Meningkatkan pelayanan pada semua bidang yang maksimal kepada warga masyarakat
                                        desa Sindumartani. <br>

                                        3. Meningkatkan kesejahteraan masyarakat desa dengan mewujudkan Badan Usaha Milik
                                        Desa (BUMDES) dan program lain untuk membuka lapangan kerja bagi masyarakat desa,
                                        serta meningkatkan produksi. <br> <br>

                                        <b>AMAN</b> <br>
                                        1. Menciptakan suasana aman berbasis warga dan bekerjasama dengan pihak
                                        terkait, <br>

                                        2. Meningkatkan kehidupan yang harmonis, toleran, saling menghormati dalam kehidupan
                                        berbudaya dan beragama di desa Sindumartani <br> <br>

                                        <b>SEHAT</b> <br>
                                        Mempertahankan dan Meningkatkan kesehatan warga dan lingkungannya melalui
                                        program “lingkungan sehat warga sehat”. <br> <br>

                                        <b>CERDAS</b> <br>
                                        1. Menciptakan dan meningkatkan sarana dan prasarana pendidikan melalui
                                        “program pendidikan Sindumartani. <br>

                                        2. Meningkatkan sarana dan prasarana dari segĺqi fisik dan non fisik bidang
                                        pendidikan, kesehatan, olahraga dan kebudayaan di desa. <br> <br>

                                        <b>PRODUKTIF</b> <br>
                                        1. Mendorong tumbuhnya industri pertanian, peternakan, perikanan, dan
                                        pariwisata, melalui program pelatihan wirausaha kepada warga yang sesuai bidang dan
                                        keadaannya. <br>

                                        2. Mengadakan pelatihan wirausaha kepada warga usia produktif yang belum bekerja
                                        disektor formal (masyarakat yang membutuhkan). <br>

                                        3. Meningkatkan kesejahteraan masyarakat desa dengan mewujudkan Badan Usaha Milik
                                        Desa (BUMDes) dan program lain untuk membuka lapangan kerja bagi masyarakat desa,
                                        serta meningkatkan produksi. <br> <br>

                                        <b>BERBUDAYA</b> <br>
                                        1. Menciptakan budaya kerja yang kreatif, jujur, adil, amanah, dalam
                                        melaksanakan pemerintahan, dan budaya transparan bidang keuangan.(good governance),
                                        <br>

                                        2. Mempertahankan dan mengembangkan budaya sahih (diakui baik) yang mencerminkan
                                        kearifan lokal (local wisdom) dan menolak budaya fasid (menimbulkan kerusakan) <br>
                                        <br>

                                        <b>BERAKHLAK MULIA</b> <br>
                                        Membuat peraturan hidup bersama berbasis budaya lokal dan agama yang
                                        mendorong terbentuknya akhlak al-karimah (mulia)
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="p-view-2">
                            <div class="tab-inner">
                                <div class="event-content head-team">
                                    @php
                                        $no = 1;
                                    @endphp
                                    <h4>Dusun</h4>
                                    @foreach ($dusun as $item)
                                        <p>
                                            {{ $no++ }}. Dusun {{ $item->nama_dusun }}
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Row -->
        </div>
    </div>
    <!-- End Faq Area -->

    <!-- Start portfolio Area -->
    <div id="portfolio" class="portfolio-area area-padding fix">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Gallery</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Start Portfolio -page -->
                <div class="awesome-project-content">
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 design development">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto1.jpg"
                                        alt="" /></a>
                                <div class="add-actions text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto1.jpg">
                                            <h4>Penyaluran BLT Dana Desa Bulan ke IV Kalurahan Sindumartani</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 photo">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto2.jpg"
                                        alt="" /></a>
                                <div class="add-actions text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto2.jpg">
                                            <h4>Sosialisasi program prioritas nasional pencegahan stunting tahun 2022</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 design">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto3.jpeg"
                                        alt="" /></a>
                                <div class="add-actions text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto3.jpeg">
                                            <h4>Pelatihan Pemasaran Online dan Temu Usaha Dagang Angkatan III di Kalurahan
                                                Sindumartani</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 photo development">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto4.jpeg"
                                        alt="" /></a>
                                <div class="add-actions text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto4.jpeg">
                                            <h4>Sosialisasi Pendewasaan Usia Perkawinan/Pencegahan Perkawinan Anak</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 development">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto5.jpg"
                                        alt="" /></a>
                                <div class="add-actions text-center text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto5.jpg">
                                            <h4>Musyawarah Kalurahan RKPKal tahun 2023</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                    <!-- single-awesome-project start -->
                    <div class="col-md-4 col-sm-4 col-xs-12 design photo">
                        <div class="single-awesome-project">
                            <div class="awesome-img">
                                <a href="#"><img src="{{ asset('front') }}/home/img/gallery/foto6.jpeg"
                                        alt="" /></a>
                                <div class="add-actions text-center">
                                    <div class="project-dec">
                                        <a class="venobox" data-gall="myGallery"
                                            href="{{ asset('front') }}/home/img/gallery/foto6.jpeg">
                                            <h4>Bazar UMKM Kalurahan Sindumartani</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single-awesome-project end -->
                </div>
            </div>
        </div>
    </div>
    <!-- awesome-portfolio end -->

    <!-- Start Suscrive Area -->
    <div class="suscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs=12">
                    <div class="suscribe-text text-center">
                        <h3><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i> <i>"Bagi masyarakat yang ingin
                                mengajukan Surat Pendatang diharapkan langsung datang ke kantor kelurahan"</i></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Suscrive Area -->
@endsection
